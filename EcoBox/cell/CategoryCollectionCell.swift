//
//  CategoryCollectionCell.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class CategoryCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var backView: UIView!
    
    var model: CategoryModel!{
        didSet{
            label.text = model.title
            updateColor()
        }
    }
    
    func click() -> Bool{
        model.categorySelected = !model.categorySelected
        
        UIView.animate(withDuration: 0.1) {
            self.updateColor()
        }
        
        return model.categorySelected
    }
    
    func updateColor() {
        if self.model.categorySelected {
            self.label.textColor = .white
            self.backView.backgroundColor = #colorLiteral(red: 0.08235294118, green: 0.5921568627, blue: 0.4078431373, alpha: 1)
            
        }else{
            self.label.textColor = .white
            self.backView.backgroundColor = #colorLiteral(red: 0.6000000238, green: 0.6000000238, blue: 0.6000000238, alpha: 1)
        }
    }
    
}
