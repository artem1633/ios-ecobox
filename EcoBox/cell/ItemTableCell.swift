//
//  ItemTableCell.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class ItemTableCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var addressLAbel: UILabel!
    @IBOutlet weak var desciptionLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var costLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var imageLogo: UIImageView!
    @IBOutlet weak var editButton: UIButton!
    
    
    var vc : UIViewController!
    var model : ItemModel!{
        didSet{
            nameLabel.text = model.name
            addressLAbel.text = model.address
            desciptionLabel.text = model.comment
            typeLabel.text = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"
            costLabel.text = model.price + "₽  "
            weightLabel.text = model.weight
            dateLabel.text = model.date
            
            favouriteButton.setImage(model.isFavourite ?  #imageLiteral(resourceName: "heart_active") : #imageLiteral(resourceName: "like_green"), for: .normal)
            
            print("item : \(model.photos.count)")
            if !model.photos.isEmpty {
                imageLogo.load(url: model.photos[0])
            }else{
                imageLogo.image = #imageLiteral(resourceName: "img_empty")
            }
        }
    
    }
    
    func showEditButton(){
        editButton.isHidden = false
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    @IBAction func favouriteButtonClicked(_ sender: Any) {
        vc.changeItemSelected(applicationId: model.id, statusAdd: !model.isFavourite) { (result) in
            if result{
                self.favouriteButton.setImage(#imageLiteral(resourceName: "heart_active"), for: .normal)
                self.model.isFavourite = true
            }else{
                self.favouriteButton.setImage(#imageLiteral(resourceName: "like_green"), for: .normal)
                self.model.isFavourite = false
            }
            self.vc.viewWillAppear(true)
        }
    }
    
    
    @IBAction func editButtonClicked(_ sender: Any) {
        editAction(model)
    }
    
    func editAction(_ model : ItemModel){
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        // Instantiate View Controller
        
        var viewController = storyboard.instantiateViewController(withIdentifier: "editItem") as! CreateItemStep1VC
        viewController.model = model
        viewController.isEditable = true
        var navController = UINavigationController(rootViewController: viewController)
        navController.modalPresentationStyle = .fullScreen
        
        self.vc.present(navController, animated: true, completion: nil)
        
        
    }
    
}
