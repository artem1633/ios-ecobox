//
//  CreateImageCollectionCell.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON

protocol ImageCollectionDelegate {
    func removeCell(index : IndexPath)
}

class CreateImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var model : UIImage!{
        didSet{
            imageView.image = model
        }
    }
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
    }
    
}
