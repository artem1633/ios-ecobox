//
//  MessageIncomeTableViewCell.swift
//  EcoBox
//
//  Created by Abdullox on 10/26/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class MessageIncomeTableViewCell: UITableViewCell {

    
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    var model: UserMessageModel!{
        didSet{
            messageLabel.text = model.content
            timeLabel.text = model.time
            userAvatar.load(url: model.avatar)
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
