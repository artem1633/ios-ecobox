//
//  ChatMessageTableCell.swift
//  EcoBox
//
//  Created by Abdullox on 8/30/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//


import UIKit

class ChatMessageTableCell: UITableViewCell {

    let messageLabel = UILabel()
    let bubbleBackgroundView = UIView()
    
    var messageLeadingAnchor : NSLayoutConstraint!
    var messageTrailingAnchor : NSLayoutConstraint!
    
    
    var model: UserMessageModel!{
        didSet{
            messageLabel.text = model.content
            bubbleBackgroundView.backgroundColor =  #colorLiteral(red: 0, green: 0.4784313725, blue: 0.9607843137, alpha: 1)
            messageLabel.textColor = .white
            
            messageTrailingAnchor.isActive = true
            
        }
    }
    
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        backgroundColor = .clear
        
        bubbleBackgroundView.layer.cornerRadius = 16
        bubbleBackgroundView.translatesAutoresizingMaskIntoConstraints = false
        addSubview(bubbleBackgroundView)
        
        addSubview(messageLabel)
        messageLabel.font = UIFont.systemFont(ofSize: 15)
        messageLabel.numberOfLines = 0
        messageLabel.translatesAutoresizingMaskIntoConstraints = false
        
        
        //make some constraints for our text
        let constraints = [
            
            messageLabel.topAnchor.constraint(equalTo: bubbleBackgroundView.topAnchor, constant: 7),
            messageLabel.leadingAnchor.constraint(equalTo: bubbleBackgroundView.leadingAnchor, constant: 12),
            messageLabel.bottomAnchor.constraint(equalTo: bubbleBackgroundView.bottomAnchor, constant: -9),
            messageLabel.trailingAnchor.constraint(equalTo: bubbleBackgroundView.trailingAnchor, constant: -12),
            
            
            bubbleBackgroundView.topAnchor.constraint(equalTo: topAnchor, constant: 12),
            bubbleBackgroundView.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 4),
            bubbleBackgroundView.widthAnchor.constraint(lessThanOrEqualToConstant: self.frame.width)
            
        ]
        
        NSLayoutConstraint.activate(constraints)
        
        messageLeadingAnchor = bubbleBackgroundView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: 24)
        messageTrailingAnchor = bubbleBackgroundView.trailingAnchor.constraint(equalTo: trailingAnchor, constant: -24)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
