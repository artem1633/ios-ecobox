//
//  ReviewTableCell.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class ReviewTableCell: UITableViewCell {
    @IBOutlet weak var userName: UILabel!
    @IBOutlet weak var comment: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var starStack: UIStackView!
    @IBOutlet weak var dateLabel: UILabel!
    
    
    var model : ReviewModel!{
        didSet{
            userName.text = model.userName
            comment.text = model.text
            typeLabel.text = model.type
            dateLabel.text = model.date
            
            let range : Int = Int(model.balls) ?? 1
            for i in 0..<range{
                let image = UIImageView(frame: CGRect(x: 20 * i, y: 0, width: 20, height: 20))
                image.image = #imageLiteral(resourceName: "star")
                starStack.addSubview(image)
            }
            
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
}
