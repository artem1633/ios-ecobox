//
//  ImageCollectionCell.swift
//  EcoBox
//
//  Created by Abdullox on 9/3/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class ImageCollectionCell: UICollectionViewCell {
    
    @IBOutlet weak var imageView: UIImageView!
    
    var imageSrc: String!{
        didSet{
            imageView.load(url: imageSrc)
        }
    }
}
