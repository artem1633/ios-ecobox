//
//  ChatTableCell.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class ChatTableCell: UITableViewCell {
    
    @IBOutlet weak var imageLabel: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var messageLabel: UILabel!
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var readCountView: UIView!
    @IBOutlet weak var readCountLabel: UILabel!
    
    var model : ChatUserModel!{
        didSet{
            let operation = model.itemModel!.operation == "0" ? "Покупка" : model.itemModel!.operation == "1" ? "Продажа" : "Заводы"
            
            imageLabel.load(url: model.itemModel!.imgSrc)
            nameLabel.text = model.itemModel!.name
            categoryLabel.text = operation + " | " + model.itemModel!.type
            userNameLabel.text = model.userName
            messageLabel.text = model.message
            readCountLabel.text = model.readCount
            dateLabel.text = model.itemModel!.date
            
            let read : Int = Int(model.readCount) ?? 0
            print("so: \(read)")
            if read == 0{
                readCountView.isHidden = true
            }
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
