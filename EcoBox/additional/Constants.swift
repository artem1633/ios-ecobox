//
//  Constants.swift
//  BisyorTrade
//
//  Created by Azimjon Nu'monov on 7/15/19.
//  Copyright © 2019 Nodir Group. All rights reserved.
//

import Foundation

class Constants {
    public static let BASE_URL : String = "http://194.67.108.188/"
    
    public static let URL_INTRO : String = Constants.BASE_URL + "api/user/settings"
    public static let URL_FCM_TOKEN : String = Constants.BASE_URL + "api/user/set-fcm-token"
    
    public static let URL_SET_TOKEN : String = Constants.BASE_URL + "api/user/set-token"
    public static let URL_USER_DATA : String = Constants.BASE_URL + "api/user/me"
    public static let URL_USER_EDIT : String = Constants.BASE_URL + "api/user/edit"
    
    //MARK: - TASK
    public static let URL_ITEM_CREATE : String = Constants.BASE_URL + "api/application/create"
    public static let URL_ITEM_UPDATE : String = Constants.BASE_URL + "api/application/update"
    public static let URL_ITEM_UPLOAD_IMAGE : String = Constants.BASE_URL + "api/application/upload-file"
    public static let URL_ITEM_VIEW : String = Constants.BASE_URL + "api/application/view"
    
    
    //MARK: - INDEXES
    public static let URL_INDEX_CATEGORY : String = Constants.BASE_URL + "api/type/index"
    public static let URL_INDEX_WEIGHT : String = Constants.BASE_URL + "api/weight/index"
    
    
    //MARK: - FAVOURITE
    public static let URL_FAVOURITE_GET : String = Constants.BASE_URL + "api/application/index-chosen"
    public static let URL_FAVOURITE_ADD : String = Constants.BASE_URL + "api/application/chose"
    public static let URL_FAVOURITE_REMOVE : String = Constants.BASE_URL + "api/application/delete-chosen"
    
    //MARK: - SEARCHING
    public static let URL_ITEMS : String = Constants.BASE_URL + "api/application/index"

    //MARK: - MESSAGING
    public static let URL_MESSAGING_CHATS : String = Constants.BASE_URL + "api/messaging/index"
    public static let URL_MESSAGING_CREATE_CHAT : String = Constants.BASE_URL + "api/messaging/create"
    public static let URL_MESSAGING_SEND_MESSAGE : String = Constants.BASE_URL + "api/messaging/add-message"
    public static let URL_MESSAGING_MESSAGES : String = Constants.BASE_URL + "api/messaging/messages"
    
    //MARK: - Application and review
    public static let URL_ID_USER_DATA : String = Constants.BASE_URL + "api/user/properties"
    public static let URL_DELETE_ITEM : String = Constants.BASE_URL + "api/application/delete"
    public static let URL_REVIEW_SEND : String = Constants.BASE_URL + "api/review/create"
    
    
    
    
    
    //MARK: - GET TASK PART
    public static let URL_TASKS_FREE : String = Constants.BASE_URL + "api/task/free"
    public static let URL_TASKS_PAID : String = Constants.BASE_URL + "api/task/not-free"
    public static let URL_TASKS_DETAIL : String = Constants.BASE_URL + "/api/task/properties"
    
    //MARK: - GET FLAME PART
    public static let URL_FLAME_ALL : String = Constants.BASE_URL + "api/crash-disable/index"
    public static let URL_FLAME_DETAIL : String = Constants.BASE_URL + "api/crash-disable/view"
    
    
    //MARK: - GET TASK PART
    public static let URL_ODPUS_ALL : String = Constants.BASE_URL + "/api/meter/index"
    public static let URL_ODPUS_DETAIL : String = Constants.BASE_URL + "/api/meter-info/index"
    
    
    
    //MARK: - CREATE TASK PART
    public static let URL_TASK_PETETIONS : String = Constants.BASE_URL + "api/petition/index"
    public static let URL_TASK_STATUS : String = Constants.BASE_URL + "api/status/index"
    public static let URL_TASK_HOME : String = Constants.BASE_URL + "api/house/index"
    public static let URL_TASK_USERS : String = Constants.BASE_URL + "api/user/executors"
    public static let URL_TASK_CREATE : String = Constants.BASE_URL + "api/task/create"
    public static let URL_TASK_UPDATE : String = Constants.BASE_URL + "api/task/update"
    
    
    //MARK: - CREATE TASK PART
    public static let URL_HOME_ALL : String = Constants.BASE_URL + "api/house/index"
    public static let URL_HOME_DETAIL : String = Constants.BASE_URL + "api/house/properties"
    
    
    
    //MARK: - KEYS
    public static let KEY_FIRST_ENTER : String = "is_first_enter"
    public static let KEY_LOGINED : String = "is_logined"
    public static let KEY_USER_PIN : String = "key_user_pin"
    public static let KEY_USER_TOKEN : String = "key_user_token"
    public static let KEY_USER_LOGIN : String = "login"
    public static let KEY_USER_NAME : String = "name"
    public static let KEY_USER_ID : String = "id"
    public static let KEY_USER_PHONE : String = "phone"
    public static let KEY_USER_ADDRESS : String = "address"
    public static let KEY_USER_PERMISSION : String = "permission"
    public static let KEY_USER_FCM_TOKEN : String = "fcm_token"
    public static let KEY_USER_EMAIL : String = "email"
    public static let KEY_USER_CITY : String = "city"
    public static let KEY_USER_AVATAR : String = "avatar"
}
