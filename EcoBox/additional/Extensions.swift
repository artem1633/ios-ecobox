//
//  Ехтенсионс.swift
//  EcoBox
//
//  Created by Abdullox on 8/31/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation

//
//  Extensions.swift
//  AdsAsus
//
//  Created by Abdullox on 8/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage
import Alamofire
import SVProgressHUD
import SwiftyJSON

extension UIImageView{
    func load(url: String) -> Void{
        
        self.sd_setImage(with:URL(string: url))
    }
    
    func changeColor(color: UIColor){
        self.image = self.image?.withRenderingMode(.alwaysTemplate)
        self.tintColor = color
    }
    
}

extension UIViewController{
    
    func getMessageConversation(applicationId: String, userName : String = "",
                                itemName : String = "", img: String = "", category: String = "", itemModel : ItemModel){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_MESSAGING_CREATE_CHAT, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "application_id" : applicationId
        ], encoding: URLEncoding.default)
        
        makeRequest(request: req) { (result) in
            switch result{
            case .success(let data):
                if let id = data["id"].int{
                    let st = UIStoryboard(name: "Main", bundle: nil)
                    let vc = st.instantiateViewController(identifier: "userMessaging") as! MessageDetailVC
                    vc.messagingId = "\(id)"
                    vc.userName = userName
                    vc.itemModel = itemModel
                    
                    self.navigationController?.pushViewController(vc, animated: true)
                }
                
                
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func changeItemSelected(applicationId: String,statusAdd: Bool = true, favouriteResult:  @escaping(Bool) -> Void){
        var url = Constants.URL_FAVOURITE_ADD
        
        if !statusAdd {
            url = Constants.URL_FAVOURITE_REMOVE
        }
        
        print("token: \(UserDefaults.userToken)   id: \(applicationId)")
        let req = Alamofire.request(url, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "application_id" : applicationId
        ], encoding: URLEncoding.default)
        
        makeRequest(request: req) { (result) in
            switch result{
            case .success(let data):
                if let res = data["success"].bool{
                    if res {
                        if statusAdd {
                            favouriteResult(true)
                        }else{
                            favouriteResult(false)
                        }
                    }else{
                        self.changeItemSelected(applicationId: applicationId, statusAdd: !statusAdd, favouriteResult: favouriteResult)
                    }
                }
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
    }
}

extension UIImage {
    public convenience init?(color: UIColor, size: CGSize = CGSize(width: 1, height: 1)) {
        let rect = CGRect(origin: .zero, size: size)
        UIGraphicsBeginImageContextWithOptions(rect.size, false, 0.0)
        color.setFill()
        UIRectFill(rect)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
    
        guard let cgImage = image?.cgImage else { return nil }
        self.init(cgImage: cgImage)
    }
}



extension UIViewController{
    
    func shareMessage(_ model: ItemModel){
        let message = "\(model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы") " +
            "\nТип: \(model.type) " +
            "\nЦена: \(model.price) " +
            "\nГород: \(model.city) " +
            "\nАдрес: \(model.address) " +
            "\nОписание: \(model.comment) " +
            "\nТелефон: \(model.phone) " +
            "\n\nВторМа - вторсырье на карте. Покупай и продавай выгодно!" +
            "\n\nЧтобы скачать приложение перейдите по ссылке:\nДля телефона Android:" +
            "\nhttps://play.google.com/store/apps/details?id=com.teo.ecobox" +
            "\n\nДля телефона Iphone:" +
            "\nhttps://apps.apple.com/uz/app/вторма/id1537224466"
        
        let textShare = [message]
        let activityViewController = UIActivityViewController(activityItems: textShare , applicationActivities: nil)
        activityViewController.popoverPresentationController?.sourceView = self.view
        self.present(activityViewController, animated: true, completion: nil)
    }
    
    func call(phone: String){
        if let url = URL(string: "tel://\(phone)") {
            UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
    }
    
}





extension UIViewController{
    
    //MARK: - GETANOTHERUSERDATA
    func getAnotherUserData(_ userId : String, _ parser: @escaping (JSON) -> Void){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ID_USER_DATA, method: .get, parameters: ["user_id" : userId])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                parser(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
    }
    
    
}

