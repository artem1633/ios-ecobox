//
//  PaddingTextField.swift
//  EcoBox
//
//  Created by Abdullox on 10/18/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import UIKit

class PaddingTextField : UITextField{
    
    let padding = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
    
    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        return bounds.inset(by: padding)
    }
    
}
