//
//  ViewController.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class ViewController: UITabBarController, UITabBarControllerDelegate {
    
    var lastItemPosition = 0
    var newItemCreated = false

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        
        getData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        delegate = self
    }
    
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        print("selected: \(tabBarController.selectedIndex)")
        
        if tabBarController.selectedIndex == 4, newItemCreated{
            if var vcs = tabBarController.viewControllers{
                let st = UIStoryboard(name: "Main", bundle: nil)
                let vc : ProfileVC = st.instantiateViewController(withIdentifier: "profile") as! ProfileVC
                vc.initialSegment = 0
                vcs[4] = vc
                newItemCreated = false
                tabBarController.setViewControllers(vcs, animated: true)
            }
            return
        }
        
        guard lastItemPosition == 0, tabBarController.selectedIndex == 0 else {
            lastItemPosition = tabBarController.selectedIndex
            return
        }
        
        
        if var vcs = tabBarController.viewControllers{
            let st = UIStoryboard(name: "Main", bundle: nil)
            
            if vcs[0] is SearchNavCV {
                vcs[0] = st.instantiateViewController(withIdentifier: "map")
                tabBarController.setViewControllers(vcs, animated: true)
                tabBarController.tabBar.selectedItem?.image = #imageLiteral(resourceName: "menu_list")
                tabBarController.tabBar.selectedItem?.title = "Список"
            }else{
                vcs[0] = st.instantiateViewController(withIdentifier: "search")
                tabBarController.setViewControllers(vcs, animated: true)
                tabBarController.tabBar.selectedItem?.image = #imageLiteral(resourceName: "menu_map")
                tabBarController.tabBar.selectedItem?.title = "Карта"
            }
            
        }
        
        
        lastItemPosition = tabBarController.selectedIndex
    }

    func getData(){
           SVProgressHUD.show()
           
           let req = Alamofire.request(Constants.URL_USER_DATA, method: .get, parameters: ["token" : UserDefaults.userToken])
           
           makeRequest(request: req) { (response) in
               switch response{
               case .success(let data):
                   self.parseData(data: data)
                   break
               case .failure(let message):
                   self.alertMessage(message: message)
                   break
               }
           }
    }
    
    func parseData(data : JSON){
        if let id = data["id"].int{
            UserDefaults.userId = "\(id)"
        }
        
    }

}

