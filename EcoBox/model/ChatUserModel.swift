//
//  ChatUserModel.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class ChatUserModel{
    var id: String = ""
    var userName: String = ""
    var message: String = ""
    var readCount: String = ""
    var itemModel: ItemModel? = nil
    
    
    static func parse(data: JSON) -> ChatUserModel{
        let model = ChatUserModel()
        
        if let id = data["id"].string{
            model.id = id
        }
        
        if let read = data["read"].string{
            model.readCount = read
        }
       
        //Application
        guard data["application"].exists() else {
            return model
        }
        
        let app : JSON = data["application"]
        
        model.itemModel = ItemModel.parse(data: app)
        
        //Mobile User
        guard data["mobileUser"].exists() else {
            return model
        }

        let user : JSON = data["mobileUser"]
        
        if let userName = user["name"].string {
            model.userName = userName
        }
        
        guard data["lastMessage"].exists() else {
            return model
        }
        let lastMessage : JSON = data["lastMessage"]
        
        if let message = lastMessage["content"].string{
            model.message = message
        }
        
        return model
    }
    
    static func parseArray(data: [JSON]) -> [ChatUserModel]{
        
        var items : [ChatUserModel] = []
        
        for item in data{
            items.append(ChatUserModel.parse(data: item))
           }
           
           return items
       }
    
}
