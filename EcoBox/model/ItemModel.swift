//
//  ItemModel.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON
import YandexMapKit

class ItemModel{
    var id: String = ""
    var name: String = "Unknown"
    var type: String = ""
    var operation: String = ""
    var weight: String = ""
    var price: String = ""
    var phone: String = ""
    var imgSrc: String = ""
    var address: String = ""
    var city: String = ""
    var comment: String = ""
    var date: String = ""
    var weightIndex: Int = 0
    
    var coord_x: String = ""
    var coord_y: String = ""
    var point : YMKPoint? = nil
    var tag : Int = -111
    var userId: String = ""
    var user: String = ""
    var userAvatar: String = ""
    var isFavourite: Bool = false
    var photos : [String] = []
    
    
    
    static func parse(data: JSON) -> ItemModel{
        let model = ItemModel()
        
        if let id = data["id"].string{
            model.id = id
        }
        
        if let name = data["name"].string{
            model.name = name
        }
        if let id = data["created_by"].string{
            model.userId = id
        }
        if let type = data["type"].string{
            model.type = type
        }
        if let operation = data["operation"].string{
            model.operation = operation
        }
        if let city = data["city"].string{
            model.city = city
        }
        if let weight = data["weight"].string{
            model.weight = weight
         
            if weight.contains("10 000") {
                model.weightIndex = 5
            }else if weight.contains("1000"){
                if weight.contains("500") {
                    model.weightIndex = 3
                }else{
                    model.weightIndex = 4
                }
            }else if weight.contains("100"){
                if weight.contains("500") {
                    model.weightIndex = 2
                }else{
                    model.weightIndex = 1
                }
            }
    
        }
        if let price = data["price"].string{
            model.price = price
        }
        if let price = data["price"].int{
            model.price = "\(price)"
        }
        if let phone = data["phone"].string{
            model.phone = phone
        }
        if let address = data["address"].string{
            model.address = address
        }
        if let coord_x = data["coord_x"].string{
            model.coord_x = coord_x
        }
        if let coord_y = data["coord_y"].string{
            model.coord_y = coord_y
        }
        
        if let userName = data["createdBy"]["name"].string{
            model.user = userName
        }
        
        if let userAvatar = data["createdBy"]["photo"].string{
            model.userAvatar = userAvatar
        }
        
        
        if !model.coord_x.isEmpty && !model.coord_y.isEmpty {
            let lat = Double(model.coord_x)!
            let long = Double(model.coord_y)!
            
            if (lat > -89.3 && lat < 89.3) && (long > -180 && long < 180){
                model.point = YMKPoint(latitude: lat, longitude: long)
            }
            
        }
        
        if let comment = data["comment"].string{
            model.comment = comment
        }
        if let created_at = data["created_at"].string{
            model.date = created_at
        }
        if let chosen = data["chosen"].bool{
            model.isFavourite = chosen
        }
        
        if let photos = data["photos"].array{
            
            photos.forEach { (image) in
                model.photos.append(image.stringValue)
            }
            
            if !photos.isEmpty, let img = photos[0].string{
                model.imgSrc = img
            }
        }
        
        
        
        
        return model
    }
    
    static func parseArray(data: [JSON]) -> [ItemModel]{
        
        var items : [ItemModel] = []
        
        for item in data{
            items.append(ItemModel.parse(data: item))
           }
           
           return items
       }
    
}

