//
//  ReviewModel.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class ReviewModel{
    var id: String = ""
    var mobile_user_from_id : String = ""
    var mobile_user_to_id : String = ""
    var balls: String = ""
    var text: String = ""
    var userName: String = ""
    var type: String = ""
    var date: String = ""
    
    
    
    static func parse(data: JSON) -> ReviewModel{
        let model = ReviewModel()
        
        if let id = data["id"].string{
            model.id = id
        }
        
        if let mobile_user_from_id = data["mobile_user_from_id"].string{
            model.mobile_user_from_id = mobile_user_from_id
        }
        
        if let mobile_user_to_id = data["mobile_user_to_id"].string{
            model.mobile_user_to_id = mobile_user_to_id
        }
        
        if let balls = data["balls"].string{
            model.balls = balls
        }
        
        if let text = data["text"].string{
            model.text = text
        }
       
        
        guard data["mobileUserFrom"].exists() else {
            return model
        }

        let app : JSON = data["mobileUserFrom"]
        if let name = app["name"].string{
            model.userName = name
        }
        
        if let type = app["type"].string{
            model.type = type
        }
        
        if let date = app["created_at"].string{
            model.date = date
        }

        return model
    }
    
    static func parseArray(data: [JSON]) -> [ReviewModel]{
        
        var items : [ReviewModel] = []
        
        for item in data{
            items.append(ReviewModel.parse(data: item))
           }
           
           return items
       }
    
}
