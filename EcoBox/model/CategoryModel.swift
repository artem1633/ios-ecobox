//
//  CategoryModel.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON
import YandexMapKit

class CategoryModel{
    var id : Int = 1
    var title : String = ""
    var categorySelected = false
    
    static func parse(data: JSON) -> CategoryModel{
        let model = CategoryModel()
           
        if let id = data["id"].int{
               model.id = id
        }
        
        if let name = data["name"].string{
            model.title = name
        }
          
    
        return model
    }
       
       static func parseArray(data: [JSON]) -> [CategoryModel]{
           
           var items : [CategoryModel] = []
           
           for item in data{
               items.append(CategoryModel.parse(data: item))
              }
              
              return items
          }
       

    
}
