//
//  UserMessageModel.swift
//  EcoBox
//
//  Created by Abdullox on 8/30/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import Foundation
import SwiftyJSON

class UserMessageModel{
    var id : String = ""
    var content : String = ""
    var date : String = ""
    var time : String = ""
    var avatar : String = ""
    var isIncoming : Bool = false
}



extension UserMessageModel{
    
    static func parse(json: JSON) -> UserMessageModel{
        let model = UserMessageModel()
        
        if let id = json["id"].string{ model.id = id }

        if let content = json["content"].string{ model.content = content }
        
        if let datetime = json["datetime"].string{
            let formatter =  DateFormatter()
            formatter.dateFormat = "YYYY-MM-DD HH:mm:ss"
            let date = formatter.date(from: datetime)
            
            let calendar = Calendar.current
            let hour = calendar.component(.hour, from: date!)
            let minutes = calendar.component(.minute, from: date!)
            
            model.time = "\(hour):\(minutes)"
        }
        
        if let avatar = json["mobileUser"]["photo"].string{
            model.avatar = avatar
        }
        
        if let inId = json["mobile_user_id"].string{
            if let id = UserDefaults.userId{
                if id == inId {
                    model.isIncoming = false
                }else{
                    model.isIncoming = true
                }
                    
            }
        }
        
        
        
        
        
        return model
    }
    
    static func parse(json: JSON) -> [UserMessageModel]?{
        var result = [UserMessageModel]()
        
        if let items = json.array{
            for storeItem in items{
                let model: UserMessageModel = UserMessageModel.parse(json: storeItem)
                result.append(model)
            }
        }else{
            return nil
        }
        
        return result
    }
    
}
