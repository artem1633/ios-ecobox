//
//  CreateItemStep3VC.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

//
//  CreateItemStep2VC.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class CreateItemStep3VC: UIViewController {
    
    @IBOutlet weak var phoneTextField: PaddingTextField!
    @IBOutlet weak var cityTextField: PaddingTextField!
    @IBOutlet weak var addressTextField: PaddingTextField!
    
    var isEditable = false
    var model : ItemModel!
    var images : [UIImage]!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
      
        
        phoneTextField.delegate = self
        cityTextField.delegate = self
        addressTextField.delegate = self
        
        if isEditable{
            phoneTextField.text = model.phone
            cityTextField.text = model.city
            addressTextField.text = model.address
        }else{
            if let phone = UserDefaults.userPhone{
                phoneTextField.text = phone
            }
            
            if let city = UserDefaults.userCity{
                cityTextField.text = city
            }
            
            if let address = UserDefaults.userAddress{
                addressTextField.text = address
            }
            
        }
        
    }
    @IBAction func gesture(_ sender: Any) {
        view.endEditing(true)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "steps4" {
            let vc = segue.destination as! ItemStep4VC
            vc.model = model
            vc.images = images
            vc.isEditable = isEditable
        }
    }
    

    @IBAction func continueButtonClicked(_ sender: Any) {
        guard !phoneTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            
            return
        }
        
        guard !cityTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        guard !addressTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            return
        }
      
        
        model.phone = phoneTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        model.city = cityTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        model.address = addressTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        performSegue(withIdentifier: "steps4", sender: self)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}


extension CreateItemStep3VC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
}
