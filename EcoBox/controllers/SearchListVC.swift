//
//  SearchListVC.swift
//  EcoBox
//
//  Created by Abdullox on 9/2/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Foundation
import YandexMapKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import MultiSelectSegmentedControl


class SearchListVC: UIViewController, MultiSelectSegmentedControlDelegate{
    enum SearchFilter{
        case none
        case date
        case price
        case weight
    }
    
    var filterStatus : SearchFilter = .none
    var filterUpToDown = true
    
    func multiSelect(_ multiSelectSegmentedControl: MultiSelectSegmentedControl, didChange value: Bool, at index: Int) {
        
        if (multiSelectSegmentedControl.selectedSegmentIndexes.isEmpty ||
                multiSelectSegmentedControl.selectedSegmentIndexes.count == 2){
            status = .all
        }else{
            if (multiSelectSegmentedControl.selectedSegmentIndex == 0){
                status = .purchase
            }else{
                status = .sell
            }
        }
        
        getData()
    }
    
    lazy var myRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    @objc func refreshTableView(_ refreshControl: UIRefreshControl){
        myRefreshControl.endRefreshing()
        getCategories()
    }
    
    
    enum SearchStatus {
        case all
        case purchase
        case sell
    }
    
    var status : SearchStatus = .all

    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var searchButton: UIButton!
    
    @IBOutlet weak var searchView: UIView!
    @IBOutlet weak var searchField: UITextField!
    @IBOutlet weak var closeButton: UIButton!
    @IBOutlet weak var segmentView: UIView!
    
    var categories : [CategoryModel] = []
    var items : [ItemModel] = []
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.addSubview(myRefreshControl)
        
        let segmetn =  MultiSelectSegmentedControl()
        segmetn.tintColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
        segmetn.selectedBackgroundColor = #colorLiteral(red: 0.9764705882, green: 0.831372549, blue: 0.1921568627, alpha: 1)
        segmetn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        segmetn.borderWidth = 0 // Width of the dividers between segments and the border around the view.
        segmetn.borderRadius = 19
        segmetn.borderWidth = 0.1
        
        segmetn.items = ["Покупают", "Продают"]
        segmetn.selectedSegmentIndexes = []
        segmetn.delegate = self
        
        
        
        segmetn.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .selected)
        
        segmentView.addSubview(segmetn)
        segmetn.translatesAutoresizingMaskIntoConstraints = false
        
        segmetn.widthAnchor.constraint(equalToConstant: 200).isActive = true
        segmetn.heightAnchor.constraint(equalToConstant: 36).isActive = true
        segmetn.centerXAnchor.constraint(equalTo: segmentView.centerXAnchor).isActive = true
        segmetn.centerYAnchor.constraint(equalTo: segmentView.centerYAnchor).isActive = true
        

        
        searchField.delegate = self
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.tableFooterView = UIView()
        
        categoryCollection.delegate = self
        categoryCollection.dataSource = self
        categoryCollection.register(UINib(nibName: "CategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "categoryCollectionCell")
        
        categoryCollection.contentInset = UIEdgeInsets(top: 0, left: 16, bottom: 0, right: 16)
        
        tableView.register(UINib(nibName: "ItemTableCell", bundle: nil), forCellReuseIdentifier: "itemTableCell")
        
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if categories.isEmpty {
            getCategories()
        }
    }
    
    @IBAction func filtrButtonClicked(_ sender: Any) {
        
        let alert = UIAlertController(title: "Сортировка", message: "", preferredStyle: .actionSheet)
        
        let date = UIAlertAction(title: "По дате", style: .default) { (action) in
            self.filterStatus = .date
            self.filter2Step(index: 1)
            alert.dismiss(animated: true, completion: nil)
        }

        let price = UIAlertAction(title: "По стоимости", style: .default) { (action) in
            self.filterStatus = .price
            self.filter2Step(index: 2)
            alert.dismiss(animated: true, completion: nil)
        }
        
        let weight = UIAlertAction(title: "По весу", style: .default) { (action) in
            self.filterStatus = .weight
            self.filter2Step(index: 3)
            alert.dismiss(animated: true, completion: nil)
        }
        
        let cancel = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(date)
        alert.addAction(price)
        alert.addAction(weight)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    func filter2Step(index: Int){
        
        let alert = UIAlertController(title: "Сортировка", message: "", preferredStyle: .actionSheet)
        
        let up = UIAlertAction(title: "По возростанию", style: .default) { (action) in
            self.filterUpToDown = false
            self.getData()
            alert.dismiss(animated: true, completion: nil)
        }

        let down = UIAlertAction(title: "По убыванию", style: .default) { (action) in
            self.filterUpToDown = true
            self.getData()
            alert.dismiss(animated: true, completion: nil)
        }
                
        let cancel = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(up)
        alert.addAction(down)
        alert.addAction(cancel)
        
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func searchClicked(_ sender: UIButton) {
        searchField.text?.removeAll()
        searchView.isHidden = false
        items.removeAll()
        tableView.reloadData()
    }
    
    @IBAction func searchCloseChanged(_ sender: UISegmentedControl) {
        view.endEditing(true)
        searchView.isHidden = true
        items.removeAll()
        tableView.reloadData()
        getData()
    }
    
}


extension SearchListVC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        
        if  let text = textField.text, !text.isEmpty{
            searchData(text: text)
        }
        
        return true
    }
        
}


extension SearchListVC : UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "itemTableCell", for: indexPath) as? ItemTableCell{
            cell.model = items[indexPath.row]
            cell.vc = self
            return cell
        }

        // Configure the cell...

        return UITableViewCell()
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        //hide all items that does not conform status ids
        if status != .all {
            if items[indexPath.row].operation == "0" && status != .purchase {
                return 0
            }

            if items[indexPath.row].operation == "1" && status != .sell{
                return 0
            }

        }

        if categories[0].categorySelected {
            return UITableView.automaticDimension
        }else{
            //hide all items does not conform selected category ids
            for category in categories{
                if category.title == items[indexPath.row].type, category.categorySelected{
                    return UITableView.automaticDimension
                }
            }
        }

        return 0
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "itemDetailVC") as! ItemDetailVC
        vc.model = items[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [shareAction(items[indexPath.row]),
                                                     locationAction(items[indexPath.row]),
                                                     userAction(items[indexPath.row])])
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [callAction(items[indexPath.row].phone),
                                                     messageAction(items[indexPath.row])])
    }
    
}


extension SearchListVC{
    
    func getData(){
        DispatchQueue.main.async {
            self.view.endEditing(true)
        }
        
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ITEMS, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func searchData(text: String){
        view.endEditing(true)
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ITEMS, method: .get, parameters: [
            "token" : UserDefaults.userToken,
            "name" : text
        ])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        guard let array = data.array else {
            return
        }
        
        if filterStatus == .price {
            items = ItemModel.parseArray(data: array)
                .sorted {
                let f1 : Int = Int($0.price) ?? 0
                let f2 : Int = Int($1.price) ?? 0
                return f1 > f2
            }

        }else if filterStatus == .date{
            items = ItemModel.parseArray(data: array)
                .sorted {
                    return $0.date > $1.date
            }
        }else if filterStatus == .weight{
            items = ItemModel.parseArray(data: array)
                .sorted {
                    return $0.weightIndex > $1.weightIndex
            }
        }else{
            items = ItemModel.parseArray(data: array)
        }
        
        
        if !filterUpToDown {
            items = items.reversed()
        }
        
        
        tableView.reloadData()
    }
    
    
    func getCategories(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_INDEX_CATEGORY, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseCategories(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseCategories(_ data: JSON){
        
        if let array = data.array{
            categories = CategoryModel.parseArray(data: array)
        }
        
        let categoryAll = CategoryModel()
        categoryAll.id = -100
        categoryAll.title = "Все типы"
        categoryAll.categorySelected = true
        
        categories.insert(categoryAll, at: 0)
        
        categoryCollection.reloadData()
        getData()
    }
    
   
}




extension SearchListVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        
        cell.model = categories[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            for (index, element) in categories.enumerated() {
                categories[index].categorySelected = false
            }
            categories[0].categorySelected = true
        }else{
            categories[indexPath.row].categorySelected = !categories[indexPath.row].categorySelected
            categories[0].categorySelected = false
        }
        
        categoryCollection.reloadData()
        tableView.reloadData()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

       guard let cell: CategoryCollectionCell =
        
            Bundle.main.loadNibNamed("CategoryCollectionCell",
                                     owner: self,
                                     options: nil)?.first as? CategoryCollectionCell else {
                                                                                    return CGSize.zero
        }
        
        
            
        cell.model = categories[indexPath.row]
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width, height: 36)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 12.0
    }
    
}
