//
//  ItemStep4VC.swift
//  EcoBox
//
//  Created by Abdullox on 10/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Foundation
import YandexMapKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import CoreLocation

class ItemStep4VC: UIViewController, CLLocationManagerDelegate{
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var myMap: YMKMapView!
    
    private var shadowLayer: CAShapeLayer!
    
    var isEditable = false
    var model : ItemModel!
    var images : [UIImage]!
    
    var selectedPoint : YMKPoint? = nil
    var userLocation : YMKPoint? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        myMap.mapWindow.map.isRotateGesturesEnabled = false
        myMap.mapWindow.map.addTapListener(with: self)
        myMap.mapWindow.map.addInputListener(with: self)
        
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        if isEditable{
            guard !model.coord_x.isEmpty, !model.coord_y.isEmpty else {
                return
            }
            
            let cordX = Double(model!.coord_x) ?? 0.0
            let cordY = Double(model!.coord_y) ?? 0.0
            let point = YMKPoint(latitude: cordX, longitude: cordY)
            selectedPoint = point
            addObject(point: point)
            
            myMap.mapWindow.map.move(with:
                YMKCameraPosition(target: point, zoom: 14, azimuth: 0, tilt: 0))
        }
            
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStep5" {
            let vc = segue.destination as! CreateItemStep5VC
            vc.model = model
            vc.images = images
            vc.isEditable = isEditable
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        userLocation = YMKPoint(latitude: locValue.latitude, longitude: locValue.longitude)
        if selectedPoint == nil {
            selectedPoint = userLocation
        }
        setUserLocation()
        
        locationManager.stopUpdatingLocation()
    }
    
    @IBAction func continueButtonClicked(_ sender: Any) {
        if let point = selectedPoint{
            model?.coord_x = "\(point.latitude)"
            model?.coord_y = "\(point.longitude)"
            performSegue(withIdentifier: "toStep5", sender: self)
        }else{
            alertMessage(message: " Выберите Геопозицию")
        }
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
}


extension ItemStep4VC : YMKLayersGeoObjectTapListener,YMKMapInputListener, YMKMapObjectTapListener, YMKUserLocationObjectListener {
    
    func onObjectAdded(with view: YMKUserLocationView) {
        view.arrow.setIconWith(#imageLiteral(resourceName: "location_cell"))
        
        selectedPoint = view.pin.geometry
        
        let pinPlacemark = view.pin.useCompositeIcon()
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {}
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {}
    
    func setUserLocation(){
        
        if let userPoint = userLocation{
            let objects = myMap.mapWindow.map.mapObjects

            myMap.mapWindow.map.move(with:
                YMKCameraPosition(target: userPoint, zoom: 14, azimuth: 0, tilt: 0))
            
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
        
    }
    
    
    func onObjectTap(with event: YMKGeoObjectTapEvent) -> Bool{
        
        if !event.geoObject.geometry.isEmpty{
            if let pointGeo = event.geoObject.geometry[0].point{
                addObject(point: pointGeo)
            }
        }
        
        
        return true
    }
        
    func onMapTap(with map: YMKMap, point: YMKPoint) {
        addObject(point: point)
    }
        
    func onMapLongTap(with map: YMKMap, point: YMKPoint) {
        addObject(point: point)
    }
        
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        addObject(point: point)
        
        return true
    }
    
    func addObject(point: YMKPoint){
        let objects = myMap.mapWindow.map.mapObjects
        
        objects.clear()
        
        if let userPoint = userLocation{
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            let obj = objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
        
        let object = objects.addPlacemark(with: point, image: #imageLiteral(resourceName: "map-pin"))
        object.addTapListener(with: self)
        
        selectedPoint = point
    }
    
    
}




