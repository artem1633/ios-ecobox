//
//  ItemDetailVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SVProgressHUD
import SwiftyJSON

class ItemDetailVC: UIViewController {
    
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var typeLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var phoneLabel: UILabel!
    @IBOutlet weak var addressLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var collectionView: UICollectionView!
    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var userAvatar: UIImageView!
    
    var model : ItemModel?
    var userId = ""
    var applicationId = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        

        // Do any additional setup after loading the view.
    }
    
    
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = true
        
        if let model = self.model{
            print("images: \(model.photos.count)")
            
        
            collectionView.delegate = self
            collectionView.dataSource = self
            
            nameLabel.text = model.name
            typeLabel.text = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"

            categoryLabel.text = model.type
            weightLabel.text = model.weight
            priceLabel.text = model.price + " ₽  "
            
            addressLabel.text = model.address
            commentLabel.text = model.comment
            dateLabel.text = model.date
            
            userNameLabel.text = model.user
            phoneLabel.text = model.phone
            userAvatar.load(url: model.userAvatar)
            
            favouriteButton.setImage(model.isFavourite ?  #imageLiteral(resourceName: "heart_like") : #imageLiteral(resourceName: "heart"), for: .normal)
        }else{
            getData()
        }
    }
    
  
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toUserDetail"{
            let vc = segue.destination as! AnotherUserVC
            vc.userId = userId
        }else if segue.identifier == "imageViewer"{
            let vc = segue.destination as! ImageViewerVC
            vc.images = model?.photos ?? []
        }
    }
    
    
    @IBAction func closeButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func shareButtonClicked(_ sender: Any) {
        if let model = self.model{
            shareMessage(model)
        }
    }
    @IBAction func locationButtonClicke(_ sender: Any) {
        
        if let point = model?.point{
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "mapPicker") as! MapPickerVC
            vc.model = model
            vc.showGeo = true

            navigationController?.pushViewController(vc, animated: true)
        }
    }
    @IBAction func callButtonClicked(_ sender: Any) {
        call(phone: model?.phone ?? "")
    }
    
    @IBAction func messagingButtonClicked(_ sender: Any) {
        if let model = self.model{
            let operation = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"
            getMessageConversation(applicationId: model.id, userName: model.user, itemName: model.name, img: model.imgSrc, category: operation + " | " + model.type,itemModel: model)
        }
        
        
    }
    @IBAction func userButtonClicked(_ sender: Any) {
        userId = model?.userId ?? ""
        performSegue(withIdentifier: "toUserDetail", sender: self)
    }
    
    @IBAction func favouriteButtonClicked(_ sender: Any) {
        
        if let model = self.model {
            changeItemSelected(applicationId: model.id, statusAdd: !model.isFavourite) { (result) -> Void in
                if result{
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "heart_like"), for: .normal)
                    self.model?.isFavourite = true
                }else{
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "heart"), for: .normal)
                    self.model?.isFavourite = false
                }
                
            }

        }
        
    }
    
}


extension ItemDetailVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
       func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return model?.photos.count ?? 0
       }
       
       func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
           
           let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "imageCollectionCell", for: indexPath) as! ImageCollectionCell
           
            cell.imageSrc = model?.photos[indexPath.row]
           
            return cell
       }

       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

        return CGSize(width: view.frame.width, height: 300)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        performSegue(withIdentifier: "imageViewer", sender: self)
    }
    
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
           return 0.0
       }
       
       func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
           return 0.0
       }
    
}



extension ItemDetailVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ITEM_VIEW, method: .get, parameters: [
            "token": UserDefaults.userToken,
            "id": applicationId
        ])
        
        makeRequest(request: req) { (response) in
            switch response {
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        model = ItemModel.parse(data: data)
        viewWillAppear(false)
    }
}
