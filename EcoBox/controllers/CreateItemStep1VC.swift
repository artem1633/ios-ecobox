//
//  CreateItemVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import SDWebImage

class CreateItemStep1VC: UIViewController{
    
    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var purchaseSegmentControl: UISegmentedControl!
    @IBOutlet weak var imageCollectionView: UICollectionView!
    
    @IBOutlet weak var imagesTextLabel: UILabel!
    @IBOutlet weak var imagesView: UIView!
    
    var isEditable = false
    
    //MARK: - VARIABLED
    let imagePickerController = UIImagePickerController()
    
    var model = ItemModel()
    var images : [UIImage] = []
    var maxImageCount : Int!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        maxImageCount = 8
        
        scrollView.delegate = self
        imageCollectionView.delegate = self
        imageCollectionView.dataSource = self
        nameTextField.delegate = self
        
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        if isEditable {
            nameTextField.text = model.name
            purchaseSegmentControl.selectedSegmentIndex = Int(model.operation) ?? 0

            for imgUrl in model.photos {
                let url = URL(string: imgUrl)
                DispatchQueue.global().async {
                    let data = try? Data(contentsOf: url!) //make sure your image in this url does exist, otherwise unwrap in a if let check / try-catch
                    DispatchQueue.main.async {
                        if let img = UIImage(data: data!){
                            self.images.append(img)
                            self.imageCollectionView.reloadData()
                        }
                    }
                }
            }
        }
    }
        
    func clear(){
        nameTextField.text?.removeAll()
        purchaseSegmentControl.selectedSegmentIndex = 0
        images.removeAll()
        imageCollectionView.reloadData()
    }
    
    
    @IBAction func continueButtonClicked(_ sender: Any) {
        guard !nameTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            
            return
       }
        
        guard images.count > 0 else{
            alertMessage(message: "Загрузите изображение")
            return
       }
        
        
        nameTextField.resignFirstResponder()
        view.endEditing(true)

        if !isEditable{
            model = ItemModel()
        }
        model.name = nameTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        model.operation = "\(purchaseSegmentControl.selectedSegmentIndex)"
        
        performSegue(withIdentifier: "toStep2", sender: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStep2" {
            let vc = segue.destination as! CreateItemStep2VC
            vc.model = model
            vc.images = images
            vc.isEditable = isEditable
        }
    }
   
    @IBAction func uploadButtonClicker(_ sender: Any) {
        uploadImage()
    }
    @IBAction func cancelButtonClicked(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}

extension CreateItemStep1VC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        guard let textFieldText = textField.text,
            let rangeOfTextToReplace = Range(range, in: textFieldText) else {
                return false
        }
        let substringToReplace = textFieldText[rangeOfTextToReplace]
        let count = textFieldText.count - substringToReplace.count + string.count
        return count <= 14
    }
    
}







extension CreateItemStep1VC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        if images.count == 0 {
            imagesTextLabel.isHidden = true
            imagesView.isHidden = true
        }else{
            imagesTextLabel.isHidden = false
            imagesView.isHidden = false
        }
        
        return images.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if let cell =  collectionView.dequeueReusableCell(withReuseIdentifier: "createImageCollectionCell", for: indexPath) as? CreateImageCollectionCell{
            
            cell.model = images[indexPath.row]
            
            return cell
        }
        
        
        return UICollectionViewCell()
    }
        
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt
        indexPath: IndexPath) -> CGSize {
        print("sizeForItemAt \(indexPath)")
        
        return CGSize(width: 124, height: 112)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}


extension CreateItemStep1VC : UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    //MARK: - Image Upload Stage
    
    
    func uploadImage(){
        let alert = UIAlertController(title: "Выберите опцию", message: nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "Камера", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePickerController.sourceType = .camera
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for: .camera){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .fullScreen
                    self.present(self.imagePickerController, animated: true, completion: nil)
                }
                
            }
            
        }
        
        let libraryAction = UIAlertAction(title: "Галерея", style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePickerController.sourceType = .photoLibrary
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .popover
                    self.present(self.imagePickerController, animated: true, completion: nil)
                }
                
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Отмена", style: .cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        pickerController(picker, didSelect: nil)
    }
    
    
    private func pickerController(_ controller: UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)

        if let image = image{
//            let model = UploadImageModel(image: image, isUploading: true, isSuccess: false, uploadName: "")
            images.append(image)
            imageCollectionView.reloadData()
        }
        
    }
    
}



extension CreateItemStep1VC{
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        view.endEditing(true)
    }
}

