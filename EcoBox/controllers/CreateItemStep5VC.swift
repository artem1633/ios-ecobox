//
//  CreateItemStep5VC.swift
//  EcoBox
//
//  Created by Abdullox on 10/25/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class CreateItemStep5VC: UIViewController {
    
    var isEditable = false
    var model : ItemModel!
    var images : [UIImage]!
    var counter = 0
    
    
    @IBOutlet weak var commentTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        commentTextView.delegate = self
        // Do any additional setup after loading the view.
        
        if isEditable {
            commentTextView.text = model.comment
        }
    }
    
    @IBAction func continueButtonClicked(_ sender: Any) {
        guard !commentTextView.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        model.comment = commentTextView.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        
        createItem()
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    
    @IBAction func gesture1(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func gesture2(_ sender: Any) {
        view.endEditing(true)
    }
    
}


extension CreateItemStep5VC : UITextFieldDelegate{
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
}


extension CreateItemStep5VC : UITextViewDelegate{
      func textViewShouldEndEditing(_ textView: UITextView) -> Bool {
        view.endEditing(true)
        
        return true
      }
    
}





extension CreateItemStep5VC {
    //MARK: - NETWORKING
    
    
    func createItem(){
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        
        var params : [String: Any] = [:]
        params["token"] = UserDefaults.userToken
        params["name"] = model?.name
        params["operation"] = model?.operation
        params["type"] = model?.type
        params["weight"] = model?.weight
        params["price"] = model?.price
        params["phone"] = model?.phone
        params["address"] = model?.address
        params["comment"] = model?.comment
        params["coord_x"] = model?.coord_x
        params["coord_y"] = model?.coord_y
        params["city"] = model?.city
        
        var req = Alamofire.request(Constants.URL_ITEM_CREATE, method: .post, parameters: params, encoding: URLEncoding.default)
        
        if isEditable {
            params["id"] = model.id
            req = Alamofire.request(Constants.URL_ITEM_UPDATE, method: .post, parameters: params, encoding: URLEncoding.default)
        }
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                if self.isEditable{
                    if let id = data["id"].string{
                        self.uploadImages(id)
                    }else{
                        self.alertMessage(message: "Некоторые значения не корректны")
                    }
                }else{
                    if let id = data["id"].int{
                        self.uploadImages("\(id)")
                    }else{
                        self.alertMessage(message: "Некоторые значения не корректны")
                    }
                }
                
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    
    func uploadImages(_ id: String){
        guard !isEditable else {
            self.dismiss(animated: true, completion: nil)
            return
        }
        
        
        if images.isEmpty {
            if let vc = self.navigationController?.viewControllers[0] as? CreateItemStep1VC{
                vc.clear()
            }
            goBack()
        }
        
        for image in images {
            uploadImage(image, id)
        }
    }

    
    func uploadImage(_ image: UIImage, _ id: String){
        SVProgressHUD.show()
                        
        let imgData = image.jpegData(compressionQuality: 0.8)!
                
        Alamofire.upload(multipartFormData: {
                    (multiFormData) in
                        multiFormData.append(imgData, withName: "file",fileName: "image.jpg", mimeType: "image/jpg")

            multiFormData.append(UserDefaults.userToken.data(using: String.Encoding.utf8)!, withName: "token")
            multiFormData.append(id.data(using: String.Encoding.utf8)!, withName: "applicationId")
            
                    },
                    to: Constants.URL_ITEM_UPLOAD_IMAGE, method: .post)
                    
                    { (result) in
                        print(result)
                        self.counter += 1
                        if self.counter == self.images.count{
                            SVProgressHUD.dismiss()
                            guard !self.isEditable else {
                                self.dismiss(animated: true, completion: nil)
                                return
                            }
                            
                            if let vc = self.navigationController?.viewControllers[0] as? CreateItemStep1VC{
                                vc.clear()
                            }
                            self.goBack()
                            
                        }
                        
                    }
        
    }
    
    func goBack(){
        DispatchQueue.main.async {
            if let tabBarController = self.tabBarController as? ViewController{
                tabBarController.newItemCreated = true
            }
            self.tabBarController?.selectedIndex = 4
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1, execute: {
            self.navigationController?.popToRootViewController(animated: false)
        })
    }
    
}
