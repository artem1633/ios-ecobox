//
//  UserReviewsVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/31/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class UserReviewsVC: UIViewController {
    
    var reviews : [ReviewModel] = []
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var addReviewHeightConstraint: NSLayoutConstraint!
    
    var anotherUserId : String? = nil
    var anotherUser = false
    
    lazy var myRefreshControl: UIRefreshControl = {
        let refreshControl = UIRefreshControl()
        
        refreshControl.addTarget(self, action: #selector(refreshTableView(_:)), for: .valueChanged)
        
        return refreshControl
    }()
    
    @objc func refreshTableView(_ refreshControl: UIRefreshControl){
        myRefreshControl.endRefreshing()
        getData()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.addSubview(myRefreshControl)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.allowsSelection = false
        tableView.tableFooterView = UIView()
        
        tableView.register(UINib(nibName: "ReviewTableCell", bundle: nil), forCellReuseIdentifier: "reviewTableCell")

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ = anotherUserId{
            anotherUser = true
            
        }else{
            addReviewHeightConstraint.constant = 0
        }

        getData()
    }

    @IBAction func addreviewButtonClicked(_ sender: Any) {
       makeSendReview()
    }

}

extension UserReviewsVC : UITableViewDelegate, UITableViewDataSource{
    

       // MARK: - Table view data source

       func numberOfSections(in tableView: UITableView) -> Int {
           // #warning Incomplete implementation, return the number of sections
           return 1
       }

       func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           // #warning Incomplete implementation, return the number of rows
           return reviews.count
       }

       
       func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if let cell = tableView.dequeueReusableCell(withIdentifier: "reviewTableCell", for: indexPath) as? ReviewTableCell{
               cell.model = reviews[indexPath.row]
            
               return cell
           }

           // Configure the cell...

           return UITableViewCell()
       }

}



extension UserReviewsVC{
    
    func getData(){
        SVProgressHUD.show()

        var req = Alamofire.request(Constants.URL_ID_USER_DATA, method: .get, parameters: [
            "user_id" : UserDefaults.userId!,
        ])
        
        if anotherUser {
            req = Alamofire.request(Constants.URL_ID_USER_DATA, method: .get, parameters: [
                "user_id" : anotherUserId!,
            ])
        }
        
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        if let array = data["reviews"].array{
            reviews = ReviewModel.parseArray(data: array).reversed()
        }
    
        tableView.reloadData()
    }
    
}



extension UserReviewsVC{
    

    func makeSendReview(){
        var myTextField : UITextField? = nil
        
        let alert = UIAlertController(title: "Добавить отзыв", message: nil, preferredStyle: .alert)
               
        alert.addTextField { (textField) in
            myTextField = textField
        }
               
        let actionCancel = UIAlertAction(title: "Отмена", style: .destructive) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        let actionSubmit = UIAlertAction(title: "Отправить", style: .default) { (action) in
            
            if let text = myTextField?.text{
                if !text.isEmpty {
                    alert.dismiss(animated: true) {
                        self.continueReviews(text)
                    }
                }
            }
            
            
        }
        
        alert.addAction(actionCancel)
        alert.addAction(actionSubmit)
               
        present(alert, animated: true, completion: nil)
        
    }
    
    func continueReviews(_ text: String){
        
        let alert = UIAlertController(title: "Балл отзыва", message: nil, preferredStyle: .alert)
               
        let actionStar1 = UIAlertAction(title: "⭐️", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.sendReview(text, "1")
        }
        
        let actionStar2 = UIAlertAction(title: "⭐️⭐️", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.sendReview(text, "2")
        }
        
        let actionStar3 = UIAlertAction(title: "⭐️⭐️⭐️", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.sendReview(text, "3")
        }
        
        let actionStar4 = UIAlertAction(title: "⭐️⭐️⭐️⭐️", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.sendReview(text, "4")
        }
        
        let actionStar5 = UIAlertAction(title: "⭐️⭐️⭐️⭐️⭐️", style: .default) { (action) in
            alert.dismiss(animated: true, completion: nil)
            self.sendReview(text, "5")
        }
        
        let actionCancel = UIAlertAction(title: "Отмена", style: .destructive) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(actionStar5)
        alert.addAction(actionStar4)
        alert.addAction(actionStar3)
        alert.addAction(actionStar2)
        alert.addAction(actionStar1)
        alert.addAction(actionCancel)
        
        present(alert, animated: true, completion: nil)

    }

    func sendReview(_ text: String, _ balls: String){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_REVIEW_SEND, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "mobile_user_from_id" : UserDefaults.userId!,
            "mobile_user_to_id" : anotherUserId!,
            "balls" : balls,
            "text" : text,
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.getData()
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
    }
    
}
