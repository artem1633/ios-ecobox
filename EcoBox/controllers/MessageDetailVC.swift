//
//  MessageDetailVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/30/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class MessageDetailVC: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var messageEditorTextField: UITextField!
    @IBOutlet weak var sendMessageButton: UIButton!
    
    @IBOutlet weak var ediTextContainerHeightConstraint: NSLayoutConstraint!
    
    
    @IBOutlet weak var itemImg: UIImageView!
    @IBOutlet weak var itemNameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    //initial variables
    var messagingId : String!
    var itemModel : ItemModel!
    var userName : String = ""
    
    var messages = [UserMessageModel]()
    var canSendMessage = false
    

    override func viewDidLoad() {
        super.viewDidLoad()
        navigationController?.navigationItem.title = userName
        
        let operation = itemModel.operation == "0" ? "Покупка" : itemModel.operation == "1" ? "Продажа" : "Заводы"
        itemImg.load(url: itemModel.imgSrc)
        itemNameLabel.text = itemModel.name
        categoryLabel.text = operation + " | " + itemModel.type
        
        tableView.transform = CGAffineTransform(scaleX: 1, y: -1)
        
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        tableView.allowsSelection = false
        tableView.backgroundColor = UIColor(white: 1, alpha: 1)
        // Do any additional setup after loading the view.
        
        tableView.register(UINib(nibName: "MessageTableViewCell", bundle: nil), forCellReuseIdentifier: "messageTableViewCell")
        tableView.register(UINib(nibName: "MessageIncomeTableViewCell", bundle: nil), forCellReuseIdentifier: "messageIncomeTableViewCell")
        
        tableView.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 24, right: 0)

        // Do any additional setup after loading the view.
        
        someConfigurations()
        getData()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toUserDetail"{
            let vc = segue.destination as! AnotherUserVC
            vc.userId = itemModel.userId
        }
    }
    
    @IBAction func userAvatarClicked(_ sender: Any) {
        performSegue(withIdentifier: "toUserDetail", sender: self)
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
        self.tabBarController?.tabBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.tabBarController?.tabBar.isHidden = false
    }

    @IBAction func itemShowButtonClicked(_ sender: Any) {
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "itemDetailVC") as! ItemDetailVC
        vc.model = itemModel
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    
    func someConfigurations(){
        
        messageEditorTextField.layer.cornerRadius = 8
        messageEditorTextField.delegate = self
        
        let paddingView: UIView = UIView(frame: CGRect(x: 0, y: 0, width: 8, height: 1))
        messageEditorTextField.leftView = paddingView
        messageEditorTextField.leftViewMode = .always
        
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillShowNotification, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(handleKeyboardNotification), name: UIResponder.keyboardWillHideNotification, object: nil)
        
        
        
        
    }
    
    @objc func handleKeyboardNotification(notification: NSNotification){
        
        if let userInfo = notification.userInfo{
            let keyboardSize = userInfo[UIResponder.keyboardFrameEndUserInfoKey] as? CGRect
            
            let isKeyboardShowing = notification.name == UIResponder.keyboardWillShowNotification
            let height: CGFloat = keyboardSize?.height ?? 0
            
            self.ediTextContainerHeightConstraint.constant = isKeyboardShowing ? -height : 0

            
            UIView.animate(withDuration: 0, delay: 0, options: .curveEaseOut, animations: {
                self.view.layoutIfNeeded()
            }) { (completion) in
                
                if isKeyboardShowing {
//                    self.tableView.scrollToBottom()
                }
            }
            
        }
        
    }
    
    @IBAction func sendMessageButtonClicked(_ sender: Any) {
        if canSendMessage{
            performSendMessage(message: messageEditorTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines))
        }
    }
    
}

extension MessageDetailVC : UITextFieldDelegate{
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if let text = textField.text{
            if checkEditTextForWhiteSpaces(text: text){
                canSendMessage = false
            }else{
                canSendMessage = true
            }
        }
    }
    
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if let text = textField.text{
            
            let trimmedText = text.trimmingCharacters(in: .whitespacesAndNewlines)
            if !trimmedText.isEmpty{
                performSendMessage(message: trimmedText)
            }
            
        }
        
        return false
    }
    
    
    
    func checkEditTextForWhiteSpaces(text: String) -> Bool{
        return text.trimmingCharacters(in: .whitespacesAndNewlines).isEmpty
    }
    
}

extension MessageDetailVC : UITableViewDelegate, UITableViewDataSource{
      
     //MARK: - TABLEVIEW DATASOURCE AND DELEGATE
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
         return messages.count
     }
     
     
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if messages[indexPath.row].isIncoming {
            let cell = tableView.dequeueReusableCell(withIdentifier: "messageIncomeTableViewCell", for: indexPath) as! MessageIncomeTableViewCell
            cell.model = messages[indexPath.row]
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            return cell
        }else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "messageTableViewCell", for: indexPath) as! MessageTableViewCell
            cell.model = messages[indexPath.row]
            cell.transform = CGAffineTransform(scaleX: 1, y: -1)
            return cell
        }
        
     }
     
}

extension MessageDetailVC{
    //MARK: - NETWORKING
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_MESSAGING_MESSAGES,
                                    method: .get,
                                    parameters:[
                                        "token" : UserDefaults.userToken,
                                        "messaging_id" : messagingId!
        ])
            
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
   
    }
    
    
    func performSendMessage(message: String){
        messageEditorTextField.text = ""
        messageEditorTextField.resignFirstResponder()
        
        
        let req = Alamofire.request(Constants.URL_MESSAGING_SEND_MESSAGE,
                                    method: .post,
                                    parameters:[
                                        "token" : UserDefaults.userToken,
                                        "messaging_id" : messagingId!,
                                        "content" : message
                ],
                                encoding: URLEncoding.default)
            
        makeRequest(request: req) { (response) in
                
            switch response{
            case .success(let data):
                self.getData()
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
                
            
        
        
    }
    
    //MARK: - Parsing
    func parseData(_ json: JSON){
        let result = UserMessageModel.parse(json: json) ?? [UserMessageModel]()
        messages = result.reversed()
        
        tableView.reloadData()
        tableView.scrollToBottom()
    }
}


extension UITableView {
    
    func scrollToBottom(){
        guard self.numberOfSections != 0, self.numberOfRows(inSection: 0) != 0 else {
            return
        }
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: self.numberOfRows(inSection: 0) - 1, section: 0)
            
            self.scrollToRow(at: indexPath, at: .bottom, animated: true)
        }
    }
    
    func scrollToTop() {
        guard self.numberOfSections != 0, self.numberOfRows(inSection: 0) != 0 else {
            return
        }
        
        DispatchQueue.main.async {
            let indexPath = IndexPath(row: 0, section: 0)
            self.scrollToRow(at: indexPath, at: .top, animated: false)
        }
    }
}

