//
//  UserApplicationsVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/31/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class UserApplicationsVC: UITableViewController{
    
    var items : [ItemModel] = []
    var anotherUserId : String? = nil
    var anotherUser = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ItemTableCell", bundle: nil), forCellReuseIdentifier: "itemTableCell")
    }

     override func viewWillAppear(_ animated: Bool) {
        if let _ = anotherUserId{
            anotherUser = true
        }
        
        getData()
    }

       // MARK: - Table view data source

       override func numberOfSections(in tableView: UITableView) -> Int {
           // #warning Incomplete implementation, return the number of sections
           return 1
       }

       override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
           // #warning Incomplete implementation, return the number of rows
           return items.count
       }

       
       override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
           if let cell = tableView.dequeueReusableCell(withIdentifier: "itemTableCell", for: indexPath) as? ItemTableCell{
               cell.model = items[indexPath.row]
            cell.vc = self
            cell.showEditButton()
            
               return cell
           }

           // Configure the cell...

           return UITableViewCell()
       }
       
       override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
           tableView.deselectRow(at: indexPath, animated: true)
           
           let st = UIStoryboard(name: "Main", bundle: nil)
           let vc = st.instantiateViewController(identifier: "itemDetailVC") as! ItemDetailVC
           vc.model = items[indexPath.row]
           
           navigationController?.pushViewController(vc, animated: true)
       }
       
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if !anotherUser {
            return UISwipeActionsConfiguration(actions: [deleteAction(items[indexPath.row])])
        }
        
        return UISwipeActionsConfiguration(actions: [shareAction(items[indexPath.row]),
                                                     locationAction(items[indexPath.row]),
                                                     userAction(items[indexPath.row])])
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        if !anotherUser {
            return UISwipeActionsConfiguration(actions: [locationAction(items[indexPath.row]),
                                                         shareAction(items[indexPath.row])])
        }
        
        return UISwipeActionsConfiguration(actions: [callAction(items[indexPath.row].phone),
                                                     messageAction(items[indexPath.row])])
    }

    
    func deleteAction(_ model : ItemModel) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            self.deleteItem(model)
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_delete")
        
        return action
    }
    
    
 

}


extension UserApplicationsVC{
    
    func deleteItem(_ model: ItemModel){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_DELETE_ITEM, method: .post, parameters: [
            "token" : UserDefaults.userToken,
            "id" : model.id,
        ], encoding: URLEncoding.default)
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.getData()
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
    }
    
    func getData(){
        
        SVProgressHUD.show()
        
        if anotherUser {
            getAnotherUserData(anotherUserId!) { (json) in
                self.parseData(json)
            }
        }else{
            let req = Alamofire.request(Constants.URL_ID_USER_DATA, method: .get, parameters: [
                "user_id" : UserDefaults.userId!,
            ])
            
            makeRequest(request: req) { (response) in
                
                switch response{
                case .success(let data):
                    self.parseData(data)
                    break
                case .failure(let message):
                    self.alertMessage(message: message)
                }
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        if let array = data["applications"].array{
            items = ItemModel.parseArray(data: array)
        }
        
        print(items.count)
        tableView.reloadData()
    }
    
}
