//
//  CreateItemStep2VC.swift
//  EcoBox
//
//  Created by Abdullox on 9/4/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON

class CreateItemStep2VC: UIViewController {
    
    @IBOutlet weak var categoryTextField: UITextField!
    @IBOutlet weak var weightTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    
    var isEditable = false
    var model : ItemModel!
    var images : [UIImage]!
    
    var categories : [(id: String, value: String)] = []
    var weights : [(id: String, value: String)] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    
        
        categoryTextField.delegate = self
        weightTextField.delegate = self
        priceTextField.delegate = self
        configurePickerView()
        
        if isEditable{
            categoryTextField.text = model.type
            weightTextField.text = model.weight
            priceTextField.text = model.price
        }
    }
    
    
    @IBAction func gesture(_ sender: Any) {
        view.endEditing(true)
    }
    
    @IBAction func continueButtonClicked(_ sender: Any) {
        
        guard !categoryTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            
            return
        }
        
        guard !weightTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            return
        }
        
        guard !priceTextField.text!.isEmpty else{
            alertMessage(message: "Заполните поля")
            return
        }
      
        
        model.type = categoryTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        model.weight = weightTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        model.price = priceTextField.text!.trimmingCharacters(in: .whitespacesAndNewlines)
        performSegue(withIdentifier: "toStep3", sender: self)
    }
    
    @IBAction func backButtonClicked(_ sender: Any) {
        navigationController?.popViewController(animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toStep3" {
            let vc = segue.destination as! CreateItemStep3VC
            vc.model = model
            vc.images = images
            vc.isEditable = isEditable
        }
    }
    
    func configurePickerView(){
        
        let pickerView1 = UIPickerView()
        let pickerView2 = UIPickerView()
        pickerView1.delegate = self
        pickerView1.dataSource = self
        pickerView2.delegate = self
        pickerView2.dataSource = self
        
        pickerView1.tag = 100
        pickerView2.tag = 101
        
        let toolbar = UIToolbar()
                
        toolbar.sizeToFit()
                
        let doneButton = UIBarButtonItem(title: "Готово", style: .plain, target: self, action: #selector(pickerViewDoneClicked))
                
        toolbar.setItems([doneButton], animated: false)
        toolbar.isUserInteractionEnabled = true
        
        
        categoryTextField.inputView = pickerView1
        categoryTextField.inputAccessoryView = toolbar
        
        weightTextField.inputView = pickerView2
        weightTextField.inputAccessoryView = toolbar
        
    }
    
    @objc func pickerViewDoneClicked(sender: Any){
        view.endEditing(true)
    }
}

extension CreateItemStep2VC : UIPickerViewDelegate, UIPickerViewDataSource{
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        switch pickerView.tag {
        case 100:
            return categories.count
        case 101:
            return weights.count
        default:
            return 0
        }
        
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        switch pickerView.tag {
        case 100:
            return categories[row].value
        case 101:
            return weights[row].value
        
        default:
            return ""
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        switch pickerView.tag {
        case 100:
            categoryTextField.text = categories[row].value
            break
        case 101:
            weightTextField.text = weights[row].value
            break
        
        default:
            break
        }
    }
    
    
}

extension CreateItemStep2VC : UITextFieldDelegate{
    
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        switch textField {
        case categoryTextField:
            if !categories.isEmpty {
                return true
            }else{
                getCategories()
                return false
            }
        case weightTextField:
            if !weights.isEmpty {
                return true
            }else{
                getWeights()
                return false
            }
        default:
            return true
        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
    
    
}


extension CreateItemStep2VC {
    //MARK: - NETWORKING
    
    func getCategories(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_INDEX_CATEGORY, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseCategories(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    func getWeights(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_INDEX_WEIGHT, method: .get, parameters: ["token" : UserDefaults.userToken])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseWeights(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
   
    
    //MARK: - PARSING
    
    func parseCategories(_ data : JSON){
        guard let elements = data.array else{
            return
        }
        
        for element in elements{
            categories.append((id: "\(element["id"].intValue)",
                    value: element["name"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.categoryTextField.becomeFirstResponder()
        }
        
        
    }
    
    func parseWeights(_ data : JSON){
        guard let elements = data.array else{
            return
        }
    
        for element in elements{
            weights.append((id: "\(element["id"].intValue)",
            value: element["name"].string ?? "error"))
        }
        DispatchQueue.main.async {
            self.weightTextField.becomeFirstResponder()
        }
        
    }
    
}
