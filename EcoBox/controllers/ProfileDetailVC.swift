//
//  ProfileDetailVC.swift
//  EcoBox
//
//  Created by Abdullox on 10/18/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import SVProgressHUD
import Alamofire
import SwiftyJSON
import XLPagerTabStrip

class ProfileDetailVC: UIViewController{

    @IBOutlet weak var avatarImageView: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var phoneTextField: UITextField!
    @IBOutlet weak var cityTextField: PaddingTextField!
    @IBOutlet weak var addressTextField: UITextField!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var imageLoadButton: UIButton!
    
    //        @IBOutlet weak var noAvatarImageView: UIImageView!
        
    let imagePickerController = UIImagePickerController()
    
    var anotherUserId : String? = nil
    
    var dataChanged = false
    var anotherUser = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameTextField.delegate = self
        phoneTextField.delegate = self
        addressTextField.delegate = self
        cityTextField.delegate = self
        imagePickerController.delegate = self
        imagePickerController.allowsEditing = true
        
        
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            
            imagePickerController.sourceType = .photoLibrary
            if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for:.photoLibrary){
                print(availableMediaTypes)
                imagePickerController.mediaTypes = availableMediaTypes
            }
            
        }
        
        // Do any additional setup after loading the view.
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if let _ = anotherUserId{
            anotherUser = true
            
            nameTextField.isEnabled = false
            phoneTextField.isEnabled = false
            addressTextField.isEnabled = false
    
            saveButton.isHidden = true
            imageLoadButton.isHidden = true
        }else{
            getData()
        }
    }
 
    
    @IBAction func loadAvatar(_ sender: Any) {
        pickImage()
    }
    
    @IBAction func editButtonClicked(_ sender: Any) {
        editProfile()
    }
    
    
    //MARK: - NETWORKING
    
    func editProfile(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_USER_EDIT, method:.post, parameters: [
            "token" : UserDefaults.userToken,
            "name": nameTextField.text!.trimmingCharacters(in:.whitespacesAndNewlines),
            "phone": phoneTextField.text!.trimmingCharacters(in:.whitespacesAndNewlines),
            "city": cityTextField.text!.trimmingCharacters(in:.whitespacesAndNewlines),
            "address": addressTextField.text!.trimmingCharacters(in:.whitespacesAndNewlines),
        ], encoding: URLEncoding.default)
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(_):
                self.dataChanged = false
                self.getData()
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
    }
    
    func getData(){
        SVProgressHUD.show()
        
        if anotherUser {
            getAnotherUserData(anotherUserId!) { (json) in
                let data : JSON = json["user"]
                
                if let name = data["name"].string{
                    self.nameTextField.text = name
                }
                if let phone = data["phone"].string{
                    self.phoneTextField.text = phone
                }
                if let city = data["city"].string{
                    self.cityTextField.text = city
                }
                
                if let address = data["address"].string{
                    self.addressTextField.text = address
                }
                
                if let avatar = data["photo"].string{
                    self.avatarImageView.load(url: avatar)
                }
                
            }
        }else{
            let req = Alamofire.request(Constants.URL_USER_DATA, method:.get, parameters: ["token" : UserDefaults.userToken])
            
            makeRequest(request: req) { (response) in
                switch response{
                case .success(let data):
                    self.parseData(data: data)
                    break
                case .failure(let message):
                    self.alertMessage(message: message)
                    break
                }
            }

        }
        
    }
    
    func parseData(data : JSON){
        
        if let name = data["name"].string{
            UserDefaults.userName = name
            nameTextField.text = name
        }
        if let phone = data["phone"].string{
            UserDefaults.userPhone = phone
            phoneTextField.text = phone
        }
        if let city = data["city"].string{
            UserDefaults.userCity = city
            cityTextField.text = city
        }
        
        if let address = data["address"].string{
            UserDefaults.userAddress = address
            addressTextField.text = address
        }
        
        if let avatar = data["photo"].string{
            UserDefaults.avatar = avatar
            avatarImageView.load(url: avatar)
        }
    }
}

extension ProfileDetailVC : UITextFieldDelegate{
    
    func textField(_ textField: UITextField, shouldChangeCharactersInrange: NSRange, replacementString string: String) -> Bool {
        
        dataChanged = true
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        view.endEditing(true)
        return true
    }
    
}
extension ProfileDetailVC : UIImagePickerControllerDelegate,UINavigationControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController,didFinishPickingMediaWithInfo info:[UIImagePickerController.InfoKey : Any]) {
        
        guard let image = info[.editedImage] as? UIImage else {
            return self.pickerController(picker, didSelect: nil)
        }
        self.pickerController(picker, didSelect: image)
    }
    
    func imagePickerControllerDidCancel(_ picker:UIImagePickerController) {
        pickerController(picker, didSelect: nil)
    }
    
    
    private func pickerController(_ controller:UIImagePickerController, didSelect image: UIImage?) {
        controller.dismiss(animated: true, completion: nil)
        if let image = image{
            uploadImage(image: image)
        }
        
    }
    
    
    func pickImage(){
        let alert = UIAlertController(title: "Upload Image", message:nil, preferredStyle: .actionSheet)
        
        let cameraAction = UIAlertAction(title: "From Camera", style:.default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.camera){
                
                self.imagePickerController.sourceType = .camera
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for:.camera){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .fullScreen
                    self.present(self.imagePickerController, animated:true, completion: nil)
                }
                
            }
            
        }
        
        let libraryAction = UIAlertAction(title: "From Library",style: .default) { (action) in
            
            if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
                
                self.imagePickerController.sourceType = .photoLibrary
                if let availableMediaTypes = UIImagePickerController.availableMediaTypes(for:.photoLibrary){
                    print("availableMediaTypes ", availableMediaTypes)
                    self.imagePickerController.mediaTypes = availableMediaTypes
                    
                    self.imagePickerController.modalPresentationStyle = .popover
                    self.present(self.imagePickerController, animated:true, completion: nil)
                }
                
            }
            
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style:.cancel) { (action) in
            alert.dismiss(animated: true, completion: nil)
        }
        
        alert.addAction(cameraAction)
        alert.addAction(libraryAction)
        alert.addAction(cancelAction)
        
        present(alert, animated: true, completion: nil)
        
    }
    
    
    func uploadImage(image: UIImage) {
        SVProgressHUD.show()
        view.isUserInteractionEnabled = false
        
        let imgData = image.jpegData(compressionQuality: 0.2)!
        
        
        Alamofire.upload(multipartFormData: {
            (multiFormData) in
                multiFormData.append(imgData, withName:"photo",fileName: "avatar.jpg", mimeType:"image/jpg")
            multiFormData.append(UserDefaults.userToken.data(using:String.Encoding.utf8)!, withName: "token")
                            
            },
                            to: Constants.URL_USER_EDIT, method: .post)
            
            { (result) in
                switch result {
                case .success(let upload, _, _):
                    
                    upload.responseJSON { response in
                        SVProgressHUD.dismiss()
                        self.view.isUserInteractionEnabled = true
                        
                                
                        if response.result.isSuccess {
                            let data : JSON = JSON(response.result.value!)
                            
                            
                            if response.response?.statusCode != 200 {
                                self.alertMessage(message: "Somethingis wrong, check network settings")
                            }else{
                                if let avatar = data["photo"].string{
                                    UserDefaults.avatar = avatar
                                }
                                self.avatarImageView.image = image
                                
                            }
                        }else{
                            self.alertMessage(message: "Something iswrong, check network settings")
                        }
                        
                        
                        
                    }
                case .failure(let encodingError):
                    self.alertMessage(message: "Something is wrong,check network settings")
                }
        }
        
    }
    
}
