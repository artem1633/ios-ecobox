//
//  IntroVC.swift
//  EcoBox
//
//  Created by Abdullox on 9/3/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class IntroVC: UIPageViewController {
   

    @IBOutlet weak var barButtonItem: UIBarButtonItem!
    
    
    var viewControllerList : [UIViewController] = {
        
        let st = UIStoryboard(name: "Main", bundle: nil)
        
        let vc1 = st.instantiateViewController(withIdentifier: "ad1")
        
        let vc2 = st.instantiateViewController(withIdentifier: "ad2")
        
        let vc3 = st.instantiateViewController(withIdentifier: "ad3")
        
        let vc4 = st.instantiateViewController(withIdentifier: "ad4")
        
        return [vc1, vc2, vc3, vc4]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataSource = self
        
        
        if let firstVC = viewControllerList.first{
            self.setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
       
    }
    

    @IBAction func navigationButtonClicked(_ sender: Any) {
        SVProgressHUD.show()
        
        let id = getUUID()
        
        guard id != nil else {
            alertMessage(message: "Device id error")
            return
        }
        
        let req = Alamofire.request(Constants.URL_SET_TOKEN, method: .post, parameters: ["token" : id!], encoding: URLEncoding.default)
        
        
        makeRequest(request: req) { (response) in
            switch response{
                
            case .success(let data):
                
                if let res = data["result"].bool {
                    if res {
                        self.enterProgramm(id: id!)
                    }else{
                        self.alertMessage(message: "Network error")
                    }
                }
                
            case .failure(let message):
                self.alertMessage(message: message)
                break
                
            }
        }
        
    }
    
    func enterProgramm(id: String){
        setFcmToken()
        UserDefaults.userToken = id
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "ViewController") as! ViewController
        
        self.present(vc, animated: true, completion: nil)
    }
    
    func setFcmToken(){
        guard !UserDefaults.userToken.isEmpty && !UserDefaults.userFcmToken.isEmpty else{
            return
        }
        
        let req = Alamofire.request(Constants.URL_FCM_TOKEN,
                                    method: .post,
                                    parameters: ["token": UserDefaults.userToken, "fcm_token" : UserDefaults.userFcmToken],
                                    encoding: URLEncoding.default)
    
        req.responseJSON { (response) in
            if response.result.isSuccess {
                let data : JSON = JSON(response.result.value!)
                print("fcmUrl: \(data)")
                    
                if response.response?.statusCode == 200 {
                    print("fcm updated!")
                }
            }else{
                
            }
        }
        
        
    }
    
}



extension IntroVC : UIPageViewControllerDelegate, UIPageViewControllerDataSource{
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {

        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        
        let prevIndex = vcIndex - 1
        
        guard prevIndex >= 0 else {
            return nil
        }
        
        guard viewControllerList.count > prevIndex else {
            return nil
        }
        
        return viewControllerList[prevIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let vcIndex = viewControllerList.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = vcIndex + 1
        
        guard viewControllerList.count !=  nextIndex else {
            return nil
        }
        
        guard viewControllerList.count >  nextIndex else {
            return nil
        }
        
        return viewControllerList[nextIndex]
    }
    
    
    
    
}


extension UIViewController{
    /// Creates a new unique user identifier or retrieves the last one created
    func getUUID() -> String? {

        // create a keychain helper instance
        let keychain = KeychainAccess()

        // this is the key we'll use to store the uuid in the keychain
        let uuidKey = "com.myorg.myappid.unique_uuid"

        // check if we already have a uuid stored, if so return it
        if let uuid = try? keychain.queryKeychainData(itemKey: uuidKey), uuid != nil {
            return uuid
        }

        // generate a new id
        guard let newId = UIDevice.current.identifierForVendor?.uuidString else {
            return nil
        }

        // store new identifier in keychain
        try? keychain.addKeychainData(itemKey: uuidKey, itemValue: newId)

        // return new id
        return newId
    }
}
