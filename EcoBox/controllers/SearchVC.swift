//
//  SearchVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Foundation
import YandexMapKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import CoreLocation
import MultiSelectSegmentedControl

extension YMKMapObject{
    func tag() -> Int {
        return self.hashValue
    }
}

class SearchVC: UIViewController, CLLocationManagerDelegate, MultiSelectSegmentedControlDelegate{
    func multiSelect(_ multiSelectSegmentedControl: MultiSelectSegmentedControl, didChange value: Bool, at index: Int) {
        
        if (multiSelectSegmentedControl.selectedSegmentIndexes.isEmpty ||
                multiSelectSegmentedControl.selectedSegmentIndexes.count == 2){
            status = .all
        }else{
            if (multiSelectSegmentedControl.selectedSegmentIndex == 0){
                status = .purchase
            }else{
                status = .sell
            }
        }
        
        updateMap()
        
    }
    
    
    let locationManager = CLLocationManager()
    
    enum SearchStatus {
        case all
        case sell
        case purchase
    }

    @IBOutlet weak var mapView: YMKMapView!
    @IBOutlet weak var categoryCollection: UICollectionView!
    @IBOutlet weak var infoView: UIView!
    @IBOutlet weak var infoLabel: UIImageView!
    @IBOutlet weak var favouriteButton: UIButton!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var categoryLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var weightLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var infoViewConstraint: NSLayoutConstraint!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var descriptionLabel: UILabel!
    
    private var shadowLayer: CAShapeLayer!
    
    @IBOutlet weak var addProductButton: ButtonWithShadow!
    
    var status : SearchStatus = .all
    var categories : [CategoryModel] = []
    var items : [ItemModel] = []
    var selectedModel: ItemModel? = nil
    var userLocation : YMKPoint? = nil
    
    @objc func selectionChanged(_ sender: Any){
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //make gradient
        let gradientLayer = CAGradientLayer()
        gradientLayer.frame = self.gradientView.bounds
        gradientLayer.frame = CGRect(x: self.gradientView.bounds.minX, y: self.gradientView.bounds.minY, width: view.frame.width, height: self.gradientView.bounds.height)
        gradientLayer.colors = [UIColor.white.cgColor, #colorLiteral(red: 1, green: 1, blue: 1, alpha: 0).cgColor]
        
        self.gradientView.layer.insertSublayer(gradientLayer, at: 1)
        
        let segmetn =  MultiSelectSegmentedControl()
        segmetn.tintColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
        segmetn.selectedBackgroundColor = #colorLiteral(red: 0.9764705882, green: 0.831372549, blue: 0.1921568627, alpha: 1)
        segmetn.backgroundColor = #colorLiteral(red: 1, green: 1, blue: 1, alpha: 1)
        segmetn.borderWidth = 0 // Width of the dividers between segments and the border around the view.
        segmetn.borderRadius = 19
        segmetn.borderWidth = 0.1
        
        segmetn.items = ["Покупают", "Продают"]
        segmetn.selectedSegmentIndexes = []
        segmetn.delegate = self
        
        
        
        segmetn.setTitleTextAttributes([.foregroundColor: UIColor.black], for: .selected)
        
        view.addSubview(segmetn)
        segmetn.translatesAutoresizingMaskIntoConstraints = false
        
        segmetn.widthAnchor.constraint(equalToConstant: 200).isActive = true
        segmetn.heightAnchor.constraint(equalToConstant: 36).isActive = true
        segmetn.centerXAnchor.constraint(equalTo: gradientView.centerXAnchor).isActive = true
        segmetn.centerYAnchor.constraint(equalTo: gradientView.centerYAnchor).isActive = true
        
        
        
        
        categoryCollection.delegate = self
        categoryCollection.dataSource = self
        categoryCollection.register(UINib(nibName: "CategoryCollectionCell", bundle: nil), forCellWithReuseIdentifier: "categoryCollectionCell")
        categoryCollection.contentInset = UIEdgeInsets(top: 0, left: 12, bottom: 0, right: 12)

        
        mapView.mapWindow.map.isRotateGesturesEnabled = false
        mapView.mapWindow.map.addTapListener(with: self)
        mapView.mapWindow.map.addInputListener(with: self)
        
        // Do any additional setup after loading the view.
//        setUserLocation()
        // Ask for Authorisation from the User.
        self.locationManager.requestAlwaysAuthorization()

        // For use in foreground
        self.locationManager.requestWhenInUseAuthorization()

        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        
        
        let pangesture = UIPanGestureRecognizer(target: self, action: #selector(onClickPanGesture))
        infoView.addGestureRecognizer(pangesture)
        
       
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        infoViewConstraint.constant = -0
        getCategories()
        
        
    }
    
    
    
    @objc func onClickPanGesture(_ sender: UIPanGestureRecognizer) {
        guard selectedModel != nil else {
            return
        }        
        
        let translation = sender.translation(in: infoView)
        print("pointss: \(translation.y)")
        
        // Not allowing the user to drag the view upward
        

        let infoTranslation = infoView.frame.height - translation.y
        
        guard infoTranslation >= 0 && infoTranslation <= infoView.frame.height else { return }
        
        infoViewConstraint.constant = -infoTranslation
        
        // setting x as 0 because we don't want users to move the frame side ways!! Only want straight up or down
        
        infoView.frame.origin = CGPoint(x: 0, y: infoView.frame.origin.y + translation.y)

              if sender.state == .ended {
                  let dragVelocity = sender.velocity(in: infoView)
                  if dragVelocity.y >= 700 {
                      // Velocity fast enough to dismiss the uiview
                    self.infoViewConstraint.constant = -0
                    
                  } else {
                      // Set back to original position of the view controller
                      UIView.animate(withDuration: 0.5) {
                        self.infoViewConstraint.constant = -self.infoView.frame.height
                      }
                  }
              }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        userLocation = YMKPoint(latitude: locValue.latitude, longitude: locValue.longitude)
        setUserLocation()
        
        locationManager.stopUpdatingLocation()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
    }

    @IBAction func addButtonClicked(_ sender: Any) {
        DispatchQueue.main.async {
            self.tabBarController?.selectedIndex = 2
        }
    }
    
    @IBAction func messageButtonClicked(_ sender: Any) {
        if let model = self.selectedModel{
            let operation = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"
            getMessageConversation(applicationId: model.id, userName: model.user, itemName: model.name, img: model.imgSrc, category: operation + " | " + model.type, itemModel: model)
        }
    }
    
    @IBAction func callButtonClicked(_ sender: Any) {
        call(phone: selectedModel?.phone ?? "")
//        guard let url = URL(string: "tel://\(selectedModel?.phone)"),
//            UIApplication.shared.canOpenURL(url) else { return }
//
//        if #available(iOS 10, *) {
//                   UIApplication.shared.open(url)
//        } else {
//                   UIApplication.shared.openURL(url)
//        }
        
    }
    @IBAction func shareButtonClicked(_ sender: Any) {
        if let model = self.selectedModel{
            shareMessage(model)
        }
    }
    
    @IBAction func infoFavouriteButtonClicked(_ sender: Any) {
        if let model = self.selectedModel {
            changeItemSelected(applicationId: model.id, statusAdd: !model.isFavourite) { (result) -> Void in
                if result{
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "heart_active"), for: .normal)
                    model.isFavourite = true
                }else{
                    self.favouriteButton.setImage(#imageLiteral(resourceName: "like_green"), for: .normal)
                    model.isFavourite = false
                }
                
                self.updateMap()
            }
        }
    }
    
    @IBAction func infoViewButtonClicked(_ sender: Any) {
        
        if let model = self.selectedModel{
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "itemDetailVC") as! ItemDetailVC
            vc.model = selectedModel
            
            navigationController?.pushViewController(vc, animated: true)
        }
    }
}



extension SearchVC : YMKLayersGeoObjectTapListener,YMKMapInputListener, YMKMapObjectTapListener, YMKUserLocationObjectListener {
    
    func onObjectAdded(with view: YMKUserLocationView) {
        view.arrow.setIconWith(UIImage(named:"checkCircle24Px")!)
        
        let pinPlacemark = view.pin.useCompositeIcon()
    }
    
    
    func onObjectRemoved(with view: YMKUserLocationView) {}
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {}
    
    func setUserLocation(){
        if let userPoint = userLocation{
            let objects = mapView.mapWindow.map.mapObjects

            mapView.mapWindow.map.move(with:
                YMKCameraPosition(target: userPoint, zoom: 14, azimuth: 0, tilt: 0))
            
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
    }

    
    
    func onObjectTap(with event: YMKGeoObjectTapEvent) -> Bool {
            return true
    }
        
    func onMapTap(with map: YMKMap, point: YMKPoint) {
    }
        
    func onMapLongTap(with map: YMKMap, point: YMKPoint) {
    }
        
    func updateMap(){
        let objects = mapView.mapWindow.map.mapObjects
        
        objects.clear()
        
        if let userPoint = userLocation{
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            let obj = objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
        
        for item in items{
            if let _ = item.point {
                if item.operation == "0", status != .sell {
                    addMapObject(objects, item, #imageLiteral(resourceName: "pin-green"))
                }else if item.operation == "1", status != .purchase{
                    addMapObject(objects, item, #imageLiteral(resourceName: "map-pin"))
                }
                else if item.operation == "2"{
                    addMapObject(objects, item, #imageLiteral(resourceName: "map-pin"))
                }
                
            }
        }
        
    }
    
    func addMapObject(_ objs: YMKMapObjectCollection, _ model: ItemModel, _ image: UIImage){
        guard categories[0].categorySelected else {
            categories.forEach { (category) in
                
                if category.title == model.type, category.categorySelected{
                    let object = objs.addPlacemark(with: model.point!, image: image)
                    model.tag = object.tag()
                    object.addTapListener(with: self)
                }
            }
            
            return
        }
        
            
        let object = objs.addPlacemark(with: model.point!, image: image)
        model.tag = object.tag()
        object.addTapListener(with: self)
    }
    
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        print("onMapObjectTap \(mapObject.tag())")
        
        for item in items{
            if item.tag == mapObject.tag() {
                print("model is : \(item.name)  \(item.id)")
                updateInfoView(item)
            }
        }
        
        return true
    }
    
    func updateInfoView(_ model: ItemModel){
        let oper = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"
        
        infoLabel.load(url: model.imgSrc)
        selectedModel = model
        nameLabel.text = model.name
        categoryLabel.text = oper + " | " + model.type
        priceLabel.text = model.price + " ₽  "
        weightLabel.text = model.weight
        dateLabel.text = model.date
        descriptionLabel.text = model.comment
        
        favouriteButton.setImage(model.isFavourite ?  #imageLiteral(resourceName: "heart_active") : #imageLiteral(resourceName: "like_green"), for: .normal)
        
        UIView.animate(withDuration: 1) {
            self.infoViewConstraint.constant = -self.infoView.frame.height
        }
    }
}


extension SearchVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_ITEMS, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func getCategories(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_INDEX_CATEGORY, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                self.parseCategories(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseCategories(_ data: JSON){
        
        if let array = data.array{
            categories = CategoryModel.parseArray(data: array)
        }
        
        let categoryAll = CategoryModel()
        categoryAll.id = -100
        categoryAll.title = "Все типы"
        categoryAll.categorySelected = true
        
        categories.insert(categoryAll, at: 0)
        
        categoryCollection.reloadData()
        getData()
    }
    
    func parseData(_ data: JSON){
        
        if let array = data.array{
            items = ItemModel.parseArray(data: array)
        }
        
        updateMap()
    }
}




extension SearchVC : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return categories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "categoryCollectionCell", for: indexPath) as! CategoryCollectionCell
        
        cell.model = categories[indexPath.row]
        
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            for (index, element) in categories.enumerated() {
                categories[index].categorySelected = false
            }
            categories[0].categorySelected = true
        }else{
            categories[indexPath.row].categorySelected = !categories[indexPath.row].categorySelected
            categories[0].categorySelected = false
        }
        
        categoryCollection.reloadData()
        updateMap()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {

       guard let cell: CategoryCollectionCell =
        
            Bundle.main.loadNibNamed("CategoryCollectionCell",
                                     owner: self,
                                     options: nil)?.first as? CategoryCollectionCell else {
                                                                                    return CGSize.zero
        }
        
        
            
        cell.model = categories[indexPath.row]
        cell.setNeedsLayout()
        cell.layoutIfNeeded()
        let size: CGSize = cell.contentView.systemLayoutSizeFitting(UIView.layoutFittingCompressedSize)
        return CGSize(width: size.width, height: 36)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 12.0
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 12.0
    }
}
