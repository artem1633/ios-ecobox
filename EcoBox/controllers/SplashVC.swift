//
//  SplashVC.swift
//  EcoBox
//
//  Created by Abdullox on 11/6/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit

class SplashVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
            if UserDefaults.userToken.isEmpty{
                self.performSegue(withIdentifier: "toIntro", sender: self)
            }else{
                self.performSegue(withIdentifier: "toMain", sender: self)
            }
        }
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
