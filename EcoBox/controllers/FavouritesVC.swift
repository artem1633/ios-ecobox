//
//  FavouritesVC.swift
//  EcoBox
//
//  Created by Abdullox on 8/28/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//

import UIKit
import Alamofire
import SwiftyJSON
import SVProgressHUD

class FavouritesVC: UITableViewController {
    
    var items : [ItemModel] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.tableFooterView = UIView()
        tableView.register(UINib(nibName: "ItemTableCell", bundle: nil), forCellReuseIdentifier: "itemTableCell")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        navigationController?.navigationBar.isHidden = false
        getData()
    }

    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return items.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCell(withIdentifier: "itemTableCell", for: indexPath) as? ItemTableCell{
            cell.model = items[indexPath.row]
            cell.vc = self
            return cell
        }

        // Configure the cell...

        return UITableViewCell()
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        

        
        let st = UIStoryboard(name: "Main", bundle: nil)
        let vc = st.instantiateViewController(identifier: "itemDetailVC") as! ItemDetailVC
        vc.model = items[indexPath.row]
        
        navigationController?.pushViewController(vc, animated: true)
    }
    
    override func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [shareAction(items[indexPath.row]),
                                                     locationAction(items[indexPath.row]),
                                                     userAction(items[indexPath.row])])
    }
    
    override func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        return UISwipeActionsConfiguration(actions: [callAction(items[indexPath.row].phone),
                                                     messageAction(items[indexPath.row])])
    }

}



extension FavouritesVC{
    
    func getData(){
        SVProgressHUD.show()
        
        let req = Alamofire.request(Constants.URL_FAVOURITE_GET, method: .get, parameters: [
            "token" : UserDefaults.userToken,
        ])
        
        makeRequest(request: req) { (response) in
            
            switch response{
            case .success(let data):
                self.parseData(data)
                break
            case .failure(let message):
                self.alertMessage(message: message)
            }
        }
        
    }
    
    func parseData(_ data: JSON){
        
        if let array = data.array{
            items = ItemModel.parseArray(data: array)
        }
        
        print(items.count)
        tableView.reloadData()
    }
    
}



extension UIViewController{
    
    func callAction(_ phone : String) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            self.call(phone: phone)
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.4509803922, green: 0.4745098039, blue: 0.5137254902, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_call")
        
        return action
    }
    
    func messageAction(_ model : ItemModel) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            
            let operation = model.operation == "0" ? "Покупка" : model.operation == "1" ? "Продажа" : "Заводы"
            self.getMessageConversation(applicationId: model.id, userName: model.user, itemName: model.name, img: model.imgSrc, category: operation + " | " + model.type, itemModel: model)
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.2745098039, green: 0.2941176471, blue: 0.3215686275, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_message")
        
        return action
    }
    
    func shareAction(_ model : ItemModel) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            self.shareMessage(model)
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_share")
        
        return action
    }
    
    func locationAction(_ model : ItemModel) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            if let _ = model.point{
                let st = UIStoryboard(name: "Main", bundle: nil)
                let vc = st.instantiateViewController(identifier: "mapPicker") as! MapPickerVC
                vc.model = model
                vc.showGeo = true

                self.navigationController?.pushViewController(vc, animated: true)
            }
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.2745098039, green: 0.2941176471, blue: 0.3215686275, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_location")
        
        return action
    }
    
    func userAction(_ model : ItemModel) -> UIContextualAction{
        let action = UIContextualAction(style: .normal, title: "") { (act, view, completion) in
            
            let st = UIStoryboard(name: "Main", bundle: nil)
            let vc = st.instantiateViewController(identifier: "userProfile") as! AnotherUserVC
            vc.userId = model.userId

            self.navigationController?.pushViewController(vc, animated: true)
            
            completion(true)
        }
        action.backgroundColor = #colorLiteral(red: 0.2352941176, green: 0.2509803922, blue: 0.2745098039, alpha: 1)
        action.image = #imageLiteral(resourceName: "more_user")
        
        return action
    }
    
    
    
}
