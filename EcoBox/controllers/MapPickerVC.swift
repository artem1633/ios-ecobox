//
//  MapPickerVC.swift
//  EcoBox
//
//  Created by Abdullox on 9/2/20.
//  Copyright © 2020 Azimjon. All rights reserved.
//
import UIKit
import Foundation
import YandexMapKit
import Alamofire
import SwiftyJSON
import SVProgressHUD
import CoreLocation

class MapPickerVC: UIViewController, CLLocationManagerDelegate{
    
    let locationManager = CLLocationManager()
    
    @IBOutlet weak var mapView: YMKMapView!
    
    private var shadowLayer: CAShapeLayer!
    
    var model : ItemModel?
    var images : [UIImage]!
    var showGeo = false
    var counter = 0
    
    var selectedPoint : YMKPoint? = nil
    var userLocation : YMKPoint? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        mapView.mapWindow.map.isRotateGesturesEnabled = false
        mapView.mapWindow.map.addTapListener(with: self)
        mapView.mapWindow.map.addInputListener(with: self)
        
        if let model = self.model, showGeo {
            title = model.name
            navigationItem.rightBarButtonItems?.removeAll()
            
            let objects = mapView.mapWindow.map.mapObjects
            
            let object = objects.addPlacemark(with: model.point!, image: #imageLiteral(resourceName: "map-pin"))
            
            mapView.mapWindow.map.move(with:
                                        YMKCameraPosition(target: model.point!, zoom: 14, azimuth: 0, tilt: 0))
            
        }
        // Do any additional setup after loading the view.
        
        if !showGeo{
            
            self.locationManager.requestAlwaysAuthorization()

            // For use in foreground
            self.locationManager.requestWhenInUseAuthorization()

            if CLLocationManager.locationServicesEnabled() {
                locationManager.delegate = self
                locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
                locationManager.startUpdatingLocation()
            }
            
        }
        
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        
        userLocation = YMKPoint(latitude: locValue.latitude, longitude: locValue.longitude)
        setUserLocation()
        
        locationManager.stopUpdatingLocation()
    }
    
   
    
    @IBAction func createButtonClicked(_ sender: Any) {
        if let point = selectedPoint{
            continueCreating(point)
        }else{
            alertMessage(message: " Выбеите Геоозиицию")
        }
    }
}





extension MapPickerVC : YMKLayersGeoObjectTapListener,YMKMapInputListener, YMKMapObjectTapListener, YMKUserLocationObjectListener {
    
    func onObjectAdded(with view: YMKUserLocationView) {
        view.arrow.setIconWith(#imageLiteral(resourceName: "location_cell"))
        
        selectedPoint = view.pin.geometry
        
        let pinPlacemark = view.pin.useCompositeIcon()
    }
    
    func onObjectRemoved(with view: YMKUserLocationView) {}
    
    func onObjectUpdated(with view: YMKUserLocationView, event: YMKObjectEvent) {}
    
    func setUserLocation(){
        
        if let userPoint = userLocation{
            let objects = mapView.mapWindow.map.mapObjects

            mapView.mapWindow.map.move(with:
                YMKCameraPosition(target: userPoint, zoom: 14, azimuth: 0, tilt: 0))
            
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
        
    }
    
    
    func onObjectTap(with event: YMKGeoObjectTapEvent) -> Bool{
        print("some: \(event.geoObject.geometry.count)")
        
        if !event.geoObject.geometry.isEmpty{
            if let pointGeo = event.geoObject.geometry[0].point{
                addObject(point: pointGeo)
            }
        }
        
        
        
        
        return true
    }
    
    
        
    func onMapTap(with map: YMKMap, point: YMKPoint) {
        addObject(point: point)
    }
        
    func onMapLongTap(with map: YMKMap, point: YMKPoint) {
        addObject(point: point)
    }
        
    func onMapObjectTap(with mapObject: YMKMapObject, point: YMKPoint) -> Bool {
        addObject(point: point)
        
        return true
    }
    
    func addObject(point: YMKPoint){
        let objects = mapView.mapWindow.map.mapObjects
        
        objects.clear()
        
        if let userPoint = userLocation{
            let image = UIImage(systemName: "location.circle.fill")?.withRenderingMode(.alwaysTemplate)
            let obj = objects.addPlacemark(with: userPoint, image: (image?.withTintColor(.blue))!)
        }
        
        let object = objects.addPlacemark(with: point, image: #imageLiteral(resourceName: "map-pin"))
        object.addTapListener(with: self)
        
        selectedPoint = point
    }
    
    
}




extension MapPickerVC {
    //MARK: - NETWORKING
    
    
    func continueCreating(_ point: YMKPoint){
        view.isUserInteractionEnabled = false
        SVProgressHUD.show()
        
        var params : [String: Any] = [:]
        params["token"] = UserDefaults.userToken
        params["name"] = model?.name
        params["operation"] = model?.operation
        params["type"] = model?.type
        params["weight"] = model?.weight
        params["price"] = model?.price
        params["phone"] = model?.phone
        params["address"] = model?.address
        params["comment"] = model?.comment
        params["coord_x"] = point.latitude
        params["coord_y"] = point.longitude
        
        let req = Alamofire.request(Constants.URL_ITEM_CREATE, method: .post, parameters: params, encoding: URLEncoding.default)
        
        makeRequest(request: req) { (response) in
            switch response{
            case .success(let data):
                print(data)
                if let id = data["id"].int{
                    self.uploadImages(id)
                }else{
                    self.alertMessage(message: "Некоторые значения не корректны")
                }
                
                
                break
            case .failure(let message):
                self.alertMessage(message: message)
                break
            }
        }
        
    }
    
    
    func uploadImages(_ id: Int){
        if images.isEmpty {
            if let vc = self.navigationController?.viewControllers[0] as? CreateItemStep1VC{
                vc.clear()
            }
            self.navigationController?.popToRootViewController(animated: false)
        }
        
        for image in images {
            uploadImage(image, id)
        }
    }

    
    func uploadImage(_ image: UIImage, _ id: Int){
        SVProgressHUD.show()
                        
        let imgData = image.jpegData(compressionQuality: 0.8)!
                
        Alamofire.upload(multipartFormData: {
                    (multiFormData) in
                        multiFormData.append(imgData, withName: "file",fileName: "image.jpg", mimeType: "image/jpg")

            multiFormData.append(UserDefaults.userToken.data(using: String.Encoding.utf8)!, withName: "token")
            multiFormData.append("\(id)".data(using: String.Encoding.utf8)!, withName: "applicationId")
            
                    },
                    to: Constants.URL_ITEM_UPLOAD_IMAGE, method: .post)
                    
                    { (result) in
                        print(result)
                        self.counter += 1
                        if self.counter == self.images.count{
                            SVProgressHUD.dismiss()
                            if let vc = self.navigationController?.viewControllers[0] as? CreateItemStep1VC{
                                vc.clear()
                            }
                            self.navigationController?.popToRootViewController(animated: false)
                            
                        }
                        
                    }
        
    }
    
}
